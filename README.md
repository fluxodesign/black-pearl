Black Pearl (or Freebooter) is an Android app that acts as a client to DownloadDaemon. It enables user(s) to browse and search for media files from YIFY website (movies) and The Pirate Bay. It calls the daemon's REST methods via a specially crafted HttpClient that uses SSL-encrypted connection to the daemon. It ensures that no one can "eavesdrop" on the data that flies between the client and the daemon.

To build this app, you will need to install Android SDK, especially API 15 or above (Android 4.0.3 - 4.4). I use IntelliJ IDEA v13 Community Edition to develop and test this app, so the easiest path to build is to install IntelliJ IDEA and open the project, create the appropriate Run/Debug Configuration and make the project. Make sure that you select the "Allow installation of apps from unknown sources" option in your Security settings on your device.

These are some screenshots from the app:

![Movie Listing](https://lh3.googleusercontent.com/-TrArwlXSpWM/VAllI9Gm4kI/AAAAAAAABDI/eeqkTVum3vM/w303-h539-no/Screenshot_2014-09-05-17-14-34.png)  
Movie Listing  

![Movie Details](https://lh4.googleusercontent.com/qP2P-gOUP4tW-haHqVAtE4FeZy_eYotSXH6a4S1uTDI=w303-h539)  
Movie Details  

![Movie Screenshots](https://lh6.googleusercontent.com/-Shvhmpgojlc/VAllNW2z1cI/AAAAAAAABDc/l-DZRxJXKec/w303-h539-no/Screenshot_2014-09-05-17-15-13.png)  
Movie Screenshot

![TPB Categories](https://lh3.googleusercontent.com/-xithR-kYzxg/VAllKb8cfMI/AAAAAAAABDM/ka368b6Ikbg/w303-h539-no/Screenshot_2014-09-05-17-15-48.png)  
TPB Categories

![TPB Search Results](https://lh3.googleusercontent.com/-Wi5F_ek_6f8/VAllPGfFJAI/AAAAAAAABDk/mHFXIq46Bns/w303-h539-no/Screenshot_2014-09-05-17-16-21.png)  
TPB Search Results  

![Download Progress](https://lh5.googleusercontent.com/-k6UYwepftSw/VAllHCO3hMI/AAAAAAAABC8/7n5FzIRYVOY/w303-h539-no/Screenshot_2014-09-05-17-19-15.png)  
Download Progress