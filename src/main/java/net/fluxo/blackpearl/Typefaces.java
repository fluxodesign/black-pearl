/*
 * Typefaces.java
 *
 * Copyright (c) 2014 Ronald Kurniawan. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 */
package net.fluxo.blackpearl;

import android.content.Context;
import android.graphics.Typeface;

import java.util.Hashtable;

/**
 * This is a static class that loads the default typeface from the asset folder and store it
 * in a cache.
 *
 * @author Ronald Kurniawan (viper)
 * @version 0.2.6, 28/03/2014
 */
public class Typefaces {

	/**
	 * Private cache for default typeface.
	 */
	private static final Hashtable<String, Typeface> _cache = new Hashtable<>();

	/**
	 * Returns the default typeface, loading and storing it in the cache if necessary.
	 *
	 * @param c Context object
	 * @param name typeface name
	 * @return the Typeface object
	 * @throws Exception
	 */
	public static Typeface get(Context c, String name) throws Exception {
		synchronized(_cache){
			if(!_cache.containsKey(name)){
				Typeface t = Typeface.createFromAsset(
					c.getAssets(),
					String.format("fonts/%s.ttf", name)
				);
				_cache.put(name, t);
			}
			return _cache.get(name);
		}
	}
}
