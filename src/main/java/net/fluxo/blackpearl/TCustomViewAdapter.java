/*
 * TCustomViewAdapter.java
 *
 * Copyright (c) 2014 Ronald Kurniawan. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 */
package net.fluxo.blackpearl;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import net.fluxo.blackpearl.dto.TPBObjectDetails;

import java.util.List;

/**
 * BaseAdapter for Corsair Cove Fragment.
 * @author Ronald Kurniawan (viper)
 * @version 0.2.6, 17/04/14
 */
public class TCustomViewAdapter extends BaseAdapter {

	private Context _ctx;
	private Typeface _tf = null;
	private final List<TPBObjectDetails> _items;

	public TCustomViewAdapter(Context context, Typeface font, List<TPBObjectDetails> items) {
		_ctx = context;
		_tf = font;
		_items = items;
	}

	/**
	 * Returns the number of items in the list.
	 * @return number of items in the list
	 */
	@Override
	public int getCount() {
		return _items.size();
	}

	/**
	 * Returns the object at index <i>i</i> in the list.
	 * @param i index of the item be displayed
	 * @return Object
	 */
	@Override
	public Object getItem(int i) {
		return null;
	}

	/**
	 * Returns the item ID to be fetched.
	 * @param i index of the item be displayed
	 * @return item ID
	 */
	@Override
	public long getItemId(int i) {
		return 0;
	}

	/**
	 * Inflates the layout for a single item for Corsair Cove Fragment and populate the values
	 * inside the layout.
	 * @param i index of the item be displayed
	 * @param view a View object
	 * @param viewGroup a ViewGroup object
	 * @return a View object for the Fragment
	 */
	@Override
	public View getView(int i, View view, ViewGroup viewGroup) {
		LayoutInflater inflater = (LayoutInflater)_ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		@SuppressLint({"ViewHolder", "InflateParams"}) View cellView = inflater.inflate(R.layout.tpb_cell, null);
		if (cellView != null && _items.get(i) != null) {
			TextView tvCategory = (TextView)cellView.findViewById(R.id.tv_tpb_cat);
			if (_tf != null) {
				tvCategory.setTypeface(_tf);
			}
			tvCategory.setText("CATEGORY: " + Utils.getTPBCategory(_items.get(i).getType()));
			TextView tvTitle = (TextView)cellView.findViewById(R.id.tv_tpb_title);
			if (_tf != null) {
				tvTitle.setTypeface(_tf);
			}
			tvTitle.setText(_items.get(i).getTitle());
			TextView tvSize = (TextView)cellView.findViewById(R.id.tv_tpb_size);
			if (_tf != null) {
				tvSize.setTypeface(_tf);
			}
			tvSize.setText("SIZE: " + _items.get(i).getSize());
			TextView tvUploader = (TextView)cellView.findViewById(R.id.tv_tpb_uploader);
			if (_tf != null) {
				tvUploader.setTypeface(_tf);
			}
			tvUploader.setText("UPLOADER: " + _items.get(i).getUploader());
			TextView tvUploaded = (TextView)cellView.findViewById(R.id.tv_tpb_uploaded);
			if (_tf != null) {
				tvUploaded.setTypeface(_tf);
			}
			tvUploaded.setText("UPLOADED: " + _items.get(i).getUploaded());
			TextView tvSeeders = (TextView)cellView.findViewById(R.id.tv_tpb_seeders);
			if (_tf != null) {
				tvSeeders.setTypeface(_tf);
			}
			tvSeeders.setText(_ctx.getString(R.string.item_seeders) + ": " + String.valueOf(_items.get(i).getSeeders()));
			TextView tvLeechers = (TextView)cellView.findViewById(R.id.tv_tpb_leechers);
			if (_tf != null) {
				tvLeechers.setTypeface(_tf);
			}
			tvLeechers.setText(_ctx.getString(R.string.item_leechers) + ": " + String.valueOf(_items.get(i).getLeechers()));
			//notifyDataSetChanged();
		}
		return cellView;
	}
}
