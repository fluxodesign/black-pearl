/*
 * URLFragment.java
 *
 * Copyright (c) 2014 Ronald Kurniawan. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 */
package net.fluxo.blackpearl;

import android.app.Fragment;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

/**
 * This is a Fragment where user can input a bittorent URL or a Magnet URL for the daemon to
 * download. User can type the URL directly or paste the URL from the system's Clipboard.
 *
 * @author Ronald Kurniawan (viper)
 * @version 0.2.6, 17/06/2014
 */
public class URLFragment extends Fragment {

	/**
	 * Called when Fragment is created.
	 * @param savedInstanceState a Bundle object
	 */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setRetainInstance(true);
	}

	/**
	 * Called when Fragment is resumed.
	 */
	@Override
	public void onResume() {
		super.onResume();
		if (getActivity() != null) {
			Intent intent = new Intent("net.fluxo.mediaminer.FRAGMENT_CHANGE").putExtra("new_fragment", "1");
			getActivity().sendBroadcast(intent);
		}
	}

	/**
	 * Inflates the layout for this Fragment and adds listener for the input box, so that the app
	 * can control when the "Paste" button is enabled or disabled.
	 *
	 * @param inflater a LayoutInflater object
	 * @param container a ViewGroup object
	 * @param savedInstanceState a Bundle object
	 * @return a View object for the Fragment
	 */
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View view =  inflater.inflate(R.layout.url_fragment, container, false);
		if (view != null) {
			EditText et = (EditText)view.findViewById(R.id.et_magnet_url);
			et.addTextChangedListener(new TextWatcher() {
				@Override
				public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
				}

				@Override
				public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
					if (charSequence.length() == 0) {
						enablePasteButton();
					}
				}

				@Override
				public void afterTextChanged(Editable editable) {
				}
			});
		}
		return view;
	}

	/**
	 * Paste the content of the Clipboard into the input box.
	 * @param cm ClipboardManager object
	 */
	public void pasteUrl(ClipboardManager cm) {
		ClipData data = cm.getPrimaryClip();
		if (data != null && data.getItemAt(0) != null) {
			ClipData.Item item = data.getItemAt(0);
			String url = item.getText().toString();
			if (url.length() > 0 && getView() != null) {
				EditText et = (EditText) getView().findViewById(R.id.et_magnet_url);
				et.setText(url);
			} else {
				if (getActivity() != null) {
					Toast.makeText(getActivity(), R.string.warning_clipboard_empty, Toast.LENGTH_LONG).show();
				}
			}
			disablePasteButton();
		} else {
			Toast.makeText(getActivity(), R.string.warning_clipboard_empty, Toast.LENGTH_LONG).show();
		}

	}

	/**
	 * Sends the request to download the URL to the daemon.
	 */
	public void downloadUrl() {
		if (getView() != null) {
			EditText editText = (EditText)getView().findViewById(R.id.et_magnet_url);
			if (editText.getText().length() > 0) {
				String url = editText.getText().toString();
				if (!url.startsWith("magnet") && !url.endsWith(".torrent")) {
					Toast.makeText(getActivity(), R.string.warning_invalid_torrent_url, Toast.LENGTH_LONG).show();
				} else {
					MainActivity myActivity = (MainActivity)getActivity();
					myActivity.sendMessageOut(EWebServiceCommandType.CMD_ADD_TORRENT,
							url, Utils.getGeneratedID(myActivity));
				}
				editText.setText("");
			}
		}
	}

	/**
	 * Disables the "Paste" button.
	 */
	private void disablePasteButton() {
		if (getView() != null) {
			Button btnPaste = (Button) getView().findViewById(R.id.btn_paste);
			btnPaste.setEnabled(false);
		}
	}

	/**
	 * Enables the "Paste" button.
	 */
	private void enablePasteButton() {
		if (getView() != null) {
			Button btnPaste = (Button)getView().findViewById(R.id.btn_paste);
			btnPaste.setEnabled(true);
		}
	}
}
