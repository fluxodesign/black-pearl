/*
 * DownloadFragment.java
 *
 * Copyright (c) 2014 Ronald Kurniawan. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 */
package net.fluxo.blackpearl;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;

/**
 * This Fragment displays the download progress from the daemon (if there's any).
 * @author Ronald Kurniawan (viper)
 * @version 0.2.6, 30/03/14
 */
public class DownloadFragment extends Fragment {

	/**
	 * Called when the Fragment is created.
	 * @param savedInstanceState a Bundle object
	 */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setRetainInstance(true);
	}

	/**
	 * Called when the Fragment is resumed.
	 */
	@Override
	public void onResume() {
		super.onResume();
		if (getActivity() != null) {
			Intent intent = new Intent("net.fluxo.mediaminer.FRAGMENT_CHANGE").putExtra("new_fragment", "4");
			getActivity().sendBroadcast(intent);
		}
	}

	@Override
	public void onPause() {
		super.onPause();
	}

	/**
	 * Inflates the layout for this Fragment and sets the Adapter required to display the values.
	 *
	 * @param inflater a LayoutInflater object
	 * @param container a ViewGroup object
	 * @param savedInstanceState a Bundle object
	 * @return a View object representing the layout for this Fragment
	 */
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.progress_fragment, container, false);
		if (v != null) {
			GridView gv = (GridView) v.findViewById(R.id.gv_progress);
			if (Utils.getDownloadList().size() > 0) {
				gv.setAdapter(new DCustomViewAdapter(getActivity(), Utils.getDefaultFont(getActivity())));
			}
		}
		return v;
	}

	/**
	 * Refresh the content of the Fragment.
	 */
	public void refreshContent() {
		MainActivity ma = (MainActivity)getActivity();
		if (!ma.appRunning) {
			return;
		} else {
			ma.cancelProgressAlarm();
			ma.setProgressAlarm();
		}
		if (getFragmentManager() != null && ma != null && ma.appRunning) {
			getFragmentManager().beginTransaction().detach(this).attach(this).commit();
		}
	}
}
