/*
 * MainActivity.java
 *
 * Copyright (c) 2014 Ronald Kurniawan. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 */
package net.fluxo.blackpearl;

import android.annotation.SuppressLint;
import android.app.*;
import android.content.*;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.SystemClock;
import android.preference.PreferenceManager;
import android.support.v13.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.*;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;
import net.fluxo.blackpearl.dto.*;
import net.fluxo.blackpearl.interfaces.ISpinnerSelectedListener;
import net.fluxo.blackpearl.task.ChatConnectTask;
import net.fluxo.blackpearl.task.RestConnectTask;
import org.apache.commons.codec.net.URLCodec;
import org.jivesoftware.smack.*;
import org.jivesoftware.smack.packet.Message;
import org.jivesoftware.smack.packet.Presence;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * The only Activity for the App. This Activity contains an ActionBar with 4 Tabs that accommodates 4 Fragment objects
 * that shows the URL input, the YIFY items, the TPB search and Download Progress.
 *
 * @author Ronald Kurniawan (viper)
 * @version 0.2.6, 28/03/2014
 */

public class MainActivity extends FragmentActivity implements ISpinnerSelectedListener {

	private AlertDialog _prefAlertDialog = null;
	private AlertDialog _chatRetryConnectDialog = null;
	private Fragment[] _arrFragment;
	private ViewPager _myViewPager;
	private FreebooterSwipeAdapter _mySwipeAdapter;
	private static final int TAB_URL = 0;
	private static final int TAB_YIFY = 1;
	private static final int TAB_CORSAIR = 2;
	private static final int TAB_PROGRESS = 3;
	private int _yifyPage = 1;
	private int _tpbPage = -1;
	private Menu MediaMinerMenu;
	private ProgressDialog pd;
	private String _filterRating = "0";
	private String _filterQuality = "All";
	private BroadcastReceiver _commMethodChange;
	private BroadcastReceiver _fragmentChange;
	private BroadcastReceiver _progressUpdate;
	private String _commMethod;
	private int _activeFragment = 1;        // 1: URL, 2 = YIFY, 3 = TPB, 4 = Progress
	private AlarmManager _alarmManager = null;
	private PendingIntent _progressIntent = null;
	private static final long PROGRESS_INTERVAL = 8 * 1000;
	private volatile Thread _thDownloadProgressUpdater = null;
	public volatile boolean appRunning;

	// REST-related variables...
	private boolean _httpRESTStatus = false;

	// Chat-related variables...
	private boolean _isChatConnected = false;
	private XMPPConnection chatConnection = null;
	private SmackAndroid _sm;
	private ChatConnectTask _connectTask = null;
	protected Roster myRoster = null;
	private ServerChatID myServerID = null;
	private Chat myServerChat = null;

	/**
	 * Called when the activity is first created.
	 *
	 * @param savedInstanceState a Bundle object
	 */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);
		lockOrientation();

		URLFragment urlFragment = new URLFragment();
		YIFYFragment yifyFragment = new YIFYFragment();
		CorsairFragment corsairFragment = new CorsairFragment();
		DownloadFragment downloadFragment = new DownloadFragment();
		_arrFragment = new Fragment[4];
		_arrFragment[0] = urlFragment;
		_arrFragment[1] = yifyFragment;
		_arrFragment[2] = corsairFragment;
		_arrFragment[3] = downloadFragment;
		_mySwipeAdapter = new FreebooterSwipeAdapter(getFragmentManager());
		_myViewPager = (ViewPager)findViewById(R.id.freebooter_viewpager);
		_myViewPager.setOffscreenPageLimit(4);
		_myViewPager.setAdapter(_mySwipeAdapter);

		ActionBar actionBar = getActionBar();
		if (actionBar != null) {
			actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
			ActionBar.TabListener tabListener = new ActionBar.TabListener() {
				@Override
				public void onTabSelected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {
					_myViewPager.setCurrentItem(tab.getPosition());
					if (tab.getTag().equals("DownloadProgress")) {
						_activeFragment = 4;
						setProgressAlarm();
						Toast.makeText(MainActivity.this, R.string.msg_progress, Toast.LENGTH_LONG).show();
					} else if (tab.getTag().equals("Corsair")) {
						_activeFragment = 3;
						CorsairFragment cf = getCorsairFragment();
						if (cf != null && cf.isFreshLook() && cf.getSearchCategory().equals("0")) {
							cf.showAlertNoSearch();
						}
					} else if (tab.getTag().equals("YIFY")) {
						_activeFragment = 2;
					} else if (tab.getTag().equals("URL")) {
						_activeFragment = 1;
					}
					Utils.vibrate(MainActivity.this);
				}

				@Override
				public void onTabUnselected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {
				}

				@Override
				public void onTabReselected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {
				}
			};
			actionBar.addTab(actionBar.newTab().setText(R.string.tab_url).setTabListener(tabListener).setTag("URL"), true);
			actionBar.addTab(actionBar.newTab().setText(R.string.tab_yify).setTabListener(tabListener).setTag("YIFY"));
			actionBar.addTab(actionBar.newTab().setText(R.string.tab_corsair).setTabListener(tabListener).setTag("Corsair"));
			actionBar.addTab(actionBar.newTab().setText(R.string.tab_dd).setTabListener(tabListener).setTag("DownloadProgress"));

			_myViewPager.setOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
				@Override
				public void onPageSelected(int position) {
					getActionBar().setSelectedNavigationItem(position);
				}
			});
		}
		_sm = SmackAndroid.init(getApplicationContext());
		Utils.generateChatID(MainActivity.this);
	}

	/**
	 * An inner class that implements and processes the Swipe gestures between Tabs.
	 */
	class FreebooterSwipeAdapter extends FragmentPagerAdapter {
		private final int PAGE_COUNT = 4;

		public FreebooterSwipeAdapter(FragmentManager fm) {
			super(fm);
		}

		/**
		 * Returns the Fragment object with the index <i>i</i> from the array of Fragment objects.
		 * @param i index of the item to be fetched from array
		 * @return Fragment object
		 */
		@Override
		public Fragment getItem(int i) {
			return _arrFragment[i];
		}

		/**
		 * Returns the number of Fragment objects contained in this FragmentManager.
		 * @return number of Fragment objects
		 */
		@Override
		public int getCount() {
			return PAGE_COUNT;
		}

		/**
		 * Returns the title of the Tab object based on the position selected.
		 * @param position index of the Tab object
		 * @return title of the Tab object
		 */
		@Override
		public CharSequence getPageTitle(int position) {
			switch (position) {
				case TAB_URL: return getString(R.string.tab_url);
				case TAB_YIFY: return getString(R.string.tab_yify);
				case TAB_CORSAIR: return getString(R.string.tab_corsair);
				case TAB_PROGRESS: return getString(R.string.tab_dd);
			}
			return "";
		}
	}

	/**
	 * Returns the first Fragment (URLFragment)
	 * @return URLFragment object
	 */
	public URLFragment getURLFragment() {
		return (URLFragment)_mySwipeAdapter.getItem(0);
	}

	/**
	 * Returns the second Fragment (YIFYFragment)
	 * @return YIFYFragment object
	 */
	public YIFYFragment getYIFYFragment() {
		return (YIFYFragment)_mySwipeAdapter.getItem(1);
	}

	/**
	 * Returns the third Fragment (CorsairFragment)
	 * @return CorsairFragment object
	 */
	public CorsairFragment getCorsairFragment() {
		return (CorsairFragment)_mySwipeAdapter.getItem(2);
	}

	/**
	 * Returns the fourth Fragment (DownloadFragment)
	 * @return DownloadFragment object
	 */
	public DownloadFragment getDownloadFragment() {
		return (DownloadFragment)_mySwipeAdapter.getItem(3);
	}

	/**
	 * Getter method for _isChatConnected property.
	 * @return true if we are connected to XMPP server; false otherwise
	 */
	public boolean isChatConnected() {
		return _isChatConnected;
	}

	/**
	 * Setter method for _isChatConnected property.
	 * @param connected true if we are connected to XMPP server; false otherwise
	 */
	public void setChatConnected(boolean connected) {
		_isChatConnected = connected;
	}

	/**
	 * Inflates the layout of the Menu for the App, and sets the Menu.
	 * @param menu Menu to be set
	 * @return true if everything is OK; false otherwise
	 */
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.main_activity_actions, menu);
		MediaMinerMenu = menu;
		return super.onCreateOptionsMenu(menu);
	}

	/**
	 * Called when the Activity is resumed. This method will show the Preferences Activity if no user preferences are
	 * saved. It then lock the orientation, so it will stay in Portrait mode. It registers the intent receiver for
	 * FRAGMENT_CHANGE, PROGRESS_UPDATE and COMM_METHOD_CHANGED broadcasts.
	 */
	@Override
	public void onResume() {
		super.onResume();
		appRunning = true;
		invalidateOptionsMenu();

		if (!isPreferencesDefined()) {
			showEditPrefDialog();
		}

		lockOrientation();
		dismissChatRetryConnectDialog();
		// register our broadcast receiver...
		IntentFilter intentFilter = new IntentFilter("net.fluxo.mediaminer.COMM_METHOD_CHANGED");
		_commMethodChange = new BroadcastReceiver() {
			@Override
			public void onReceive(Context context, Intent intent) {
				String data = intent.getStringExtra("new_method");      // should be either 'http' or 'xmpp'
				if (data != null && data.equals("http") && _commMethod.equals("xmpp")) {
					changeCommunicationMethod(true);
				} else if (data != null && data.equals("xmpp") && _commMethod.equals("http")) {
					changeCommunicationMethod(false);
				}
			}
		};
		registerReceiver(_commMethodChange, intentFilter);
		IntentFilter activeFragmentFilter = new IntentFilter("net.fluxo.mediaminer.FRAGMENT_CHANGE");
		_fragmentChange = new BroadcastReceiver() {
			@Override
			public void onReceive(Context context, Intent intent) {
				String data = intent.getStringExtra("new_fragment");    // should be 1 (url), 2 (yify), 3 (corsair) or 4 (progress)
				if (data != null) {
					switch (data) {
						case "1":
							_activeFragment = 1;
							MediaMinerMenu.setGroupEnabled(R.id.menu_grp_enables, false);
							break;
						case "2":
							_activeFragment = 2;
							MediaMinerMenu.setGroupEnabled(R.id.menu_grp_enables, true);
							break;
						case "3":
							_activeFragment = 3;
							MediaMinerMenu.setGroupEnabled(R.id.menu_grp_enables, true);
							break;
						case "4":
							_activeFragment = 4;
							MediaMinerMenu.setGroupEnabled(R.id.menu_grp_enables, false);
							break;
					}
				}
			}
		};
		registerReceiver(_fragmentChange, activeFragmentFilter);
		IntentFilter progressUpdateFilter = new IntentFilter("net.fluxo.mediaminer.PROGRESS_UPDATE");
		_progressUpdate = new BroadcastReceiver() {
			@Override
			public void onReceive(Context context, Intent intent) {
				sendMessageOut(EWebServiceCommandType.CMD_STATUS, Utils.getGeneratedID(MainActivity.this));
			}
		};
		registerReceiver(_progressUpdate, progressUpdateFilter);
	}

	/**
	 * Sets the next System Alarm for sending a request to the daemon to obtain download progress update.
	 */
	public void setProgressAlarm() {
		Intent updateProgressIntent = new Intent("net.fluxo.mediaminer.PROGRESS_UPDATE");
		if (_alarmManager == null) {
			_progressIntent = PendingIntent.getBroadcast(MainActivity.this, 0, updateProgressIntent, PendingIntent.FLAG_CANCEL_CURRENT);
			_alarmManager = (AlarmManager)getSystemService(Context.ALARM_SERVICE);
			_alarmManager.setInexactRepeating(AlarmManager.ELAPSED_REALTIME, SystemClock.elapsedRealtime() + PROGRESS_INTERVAL,
				PROGRESS_INTERVAL, _progressIntent);
		}
	}

	/**
	 * Cancel the currently running System Alarm for Progress update.
	 */
	public void cancelProgressAlarm() {
		if (_alarmManager != null && _progressIntent != null) {
			_alarmManager.cancel(_progressIntent);
			_alarmManager = null;
			_progressIntent = null;
		}
	}

	/**
	 * Called when Activity is destroyed. If we are connecting to servers/daemon, disconnect.
	 */
	@Override
	public void onDestroy() {
		super.onDestroy();
		_sm.onDestroy();

		if (_connectTask != null) {
			_connectTask.dismissProgressDialog();
			_connectTask.disconnect();
		}
	}

	/**
	 * Called when the Activity is being paused. Unregister every broadcast receivers and cancel the Progress alarm.
	 */
	@Override
	public void onPause() {
		super.onPause();
		appRunning = false;
		// unregister our broadcast receiver..
		unregisterReceiver(_commMethodChange);
		unregisterReceiver(_fragmentChange);
		unregisterReceiver(_progressUpdate);
		if (_alarmManager != null && _progressIntent != null) {
			_alarmManager.cancel(_progressIntent);
		}
	}

	/**
	 * Called when user choose a MenuItem.
	 * @param item a MenuItem being selected
	 * @return true
	 */
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case R.id.action_filter:
				if (_activeFragment == 2) {
					showFilterDialog();
				} else if (_activeFragment == 3) {
					AlertDialog tpbFilterDialog = new TPBFilterDialog(MainActivity.this).build();
					tpbFilterDialog.show();
				}
				return true;
			case R.id.action_search:
				if (_activeFragment == 2) {
					showSearchTermDialog();
				} else if (_activeFragment == 3) {
					CorsairFragment cFragment = getCorsairFragment();
					if (cFragment != null) {
						cFragment.showSearchDialog();
					}
				}
				return true;
			case R.id.action_settings:
				showSettingsPage();
				return true;
			case R.id.action_reconnect_chat:
				connectChat();
				return true;
			default:
				return super.onOptionsItemSelected(item);
		}
	}

	/**
	 * Starts the thread for processing Download Progress Update and refresh the associated Fragment.
	 */
	public void startDownloadProgressUpdaterThread() {
		if (_thDownloadProgressUpdater != null && _thDownloadProgressUpdater.isAlive()) {
			return;
		}
		_thDownloadProgressUpdater = new Thread(new Runnable() {
			@Override
			public void run() {
				DownloadFragment df = getDownloadFragment();
				while (Utils.getDownloadList().size() > 0) {
					try{
						if (df != null && appRunning) {
							df.refreshContent();
						}
						Thread.sleep(10000);
					} catch (InterruptedException ignored) { }
				}
				_thDownloadProgressUpdater = null;
				// clean up the fragment
				if (df != null) {
					df.refreshContent();
				}
			}
		});
		_thDownloadProgressUpdater.start();
	}

	/**
	 * Displays a ProgressDialog with specified message.
	 * @param message a String to be displayed with ProgressDialog
	 */
	public void setProgressDialog(String message) {
		if (pd == null) {
			pd = new ProgressDialog(MainActivity.this);
			pd.setMessage(message);
			pd.setTitle(R.string.app_name);
			pd.setCancelable(false);
			pd.show();
		}
	}

	/**
	 * Closes the active ProgressDialog.
	 */
	public void dismissProgressDialog() {
		if (pd != null) {
			pd.dismiss();
			pd = null;
		}
	}

	/**
	 * Disables the "Settings" menu so user cannot choose it.
	 */
	public void disableSettingsMenu() {
		if (MediaMinerMenu != null) {
			MenuItem mnuSettings = MediaMinerMenu.findItem(R.id.action_settings);
			if (mnuSettings != null && mnuSettings.isEnabled()) {
				mnuSettings.setEnabled(false);
			}
		}
	}

	/**
	 * Enables the "Settings" menu so user can choose it.
	 */
	public void enableSettingsMenu() {
		if (MediaMinerMenu != null) {
			MenuItem mnuSettings = MediaMinerMenu.findItem(R.id.action_settings);
			if (mnuSettings != null && !mnuSettings.isEnabled()) {
				mnuSettings.setEnabled(true);
			}
		}
	}

	/**
	 * If the XMPP Retry Connect Dialog is active, close it. Meanwhile, re-enable the "Reconnect" menu item,
	 * allowing user to initiate a reconnect to XMPP server.
	 */
	public void dismissChatRetryConnectDialog() {
		if (_chatRetryConnectDialog != null && _chatRetryConnectDialog.isShowing()) {
			_chatRetryConnectDialog.cancel();
			_chatRetryConnectDialog = null;
			// show the menu to enable user to reconnect...
			if (MediaMinerMenu != null) {
				MenuItem mnuReconnect = MediaMinerMenu.findItem(R.id.action_reconnect_chat);
				if (mnuReconnect != null && !mnuReconnect.isVisible()) {
					mnuReconnect.setVisible(true);
				}
			}
		}
	}

	/**
	 * Checks whether user preferences are saved and exists in the SharedPreferences.
	 * @return true if user preferences exists; false otherwise
	 */
	private boolean isPreferencesDefined() {
		boolean status = true;
		SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(this);
		String myUserId = pref.getString("freebooter_pref_chat_username", "");
		String myPassword = pref.getString("freebooter_pref_chat_password", "");
		String serverId = pref.getString("freebooter_pref_chat_serverid", "");

		if (myUserId.length() == 0 || myUserId.equals("username")) {
			status = false;
		}
		if (status && (myPassword.length() == 0 || myPassword.equals("password"))) {
			status = false;
		}
		if (status && (serverId.length() == 0 || serverId.equals("serverid"))) {
			status = false;
		}
		return status;
	}

	/**
	 * Starts the Settings Activity.
	 */
	private void showSettingsPage() {
		Intent settingsIntent = new Intent(this, SettingsActivity.class);
		startActivity(settingsIntent);
	}

	/**
	 * Displays the Dialog prompting the user to set the preferences. This dialog will be shown the first time the
	 * App is started.
	 */
	private void showEditPrefDialog() {
		if (_prefAlertDialog == null || !_prefAlertDialog.isShowing()) {
			AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
			builder.setTitle(R.string.warning_preferences_title);
			builder.setMessage(R.string.warning_preferences_message)
				.setCancelable(false)
				.setPositiveButton(R.string.btn_yes, new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int id) {
						showSettingsPage();
					}
				})
				.setNegativeButton(R.string.btn_no, new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int id) {
						MainActivity.this.finish();
					}
				});
			_prefAlertDialog = builder.create();
			_prefAlertDialog.show();
		}
	}

	/**
	 * Displays the Search Dialog for YIFY titles.
	 */
	private void showSearchTermDialog() {
		LayoutInflater inflater = LayoutInflater.from(MainActivity.this);
		@SuppressLint("InflateParams") View promptView = inflater.inflate(R.layout.yify_search, null);
		AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
		builder.setView(promptView);
		if (promptView != null) {
			final EditText userInput = (EditText) promptView.findViewById(R.id.et_search_term);
			builder.setCancelable(true).setPositiveButton(R.string.action_search, new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int id) {
					if (userInput.getText() != null && userInput.getText().length() > 0) {
						String input = userInput.getText().toString();
						sendMessageOut(EWebServiceCommandType.CMD_YIFY_SEARCH, input);
					} else {
						getYifyFirstPage();
					}
				}
			}).setNegativeButton(R.string.btn_cancel, new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int id) {
					dialog.cancel();
				}
			});
			AlertDialog dialog = builder.create();
			dialog.show();
		}
	}

	/**
	 * Displays the Search Filter Dialog for YIFY items.
	 */
	private void showFilterDialog() {
		LayoutInflater inflater = LayoutInflater.from(MainActivity.this);
		@SuppressLint("InflateParams") View promptView = inflater.inflate(R.layout.yify_filter, null);
		AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
		builder.setView(promptView);
		if (promptView != null) {
			Spinner spRating = (Spinner) promptView.findViewById(R.id.spinner_rating);
			if (spRating != null) {
				spRating.setSelection(Integer.parseInt(_filterRating));
				spRating.setOnItemSelectedListener(new SpinnerItemSelectedListener(SpinnerItemSelectedListener.T_RATING, this));
			}
			Spinner spQuality = (Spinner) promptView.findViewById(R.id.spinner_quality);
			if (spQuality != null) {
				switch (_filterQuality) {
					case "All":
						spQuality.setSelection(0);
						break;
					case "720p":
						spQuality.setSelection(1);
						break;
					case "1080p":
						spQuality.setSelection(2);
						break;
					case "3D":
						spQuality.setSelection(3);
						break;
				}
				spQuality.setOnItemSelectedListener(new SpinnerItemSelectedListener(SpinnerItemSelectedListener.T_QUALITY, this));
			}
			builder.setCancelable(true).setPositiveButton(R.string.btn_set_filter, new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int id) {
					getYifyFirstPage();
				}
			}).setNegativeButton(R.string.btn_cancel_filter, new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int id) {
					_filterQuality = "All";
					_filterRating = "0";
					getYifyFirstPage();
				}
			});
			AlertDialog dialog = builder.create();
			dialog.show();
		}
	}

	/**
	 * Starts the Task for connecting to XMPP server.
	 */
	public void connectChat() {
		if (myServerID == null) {
			String serverID = PreferenceManager.getDefaultSharedPreferences(MainActivity.this).getString("freebooter_pref_chat_serverid", "");
			if (serverID != null && serverID.length() > 0) {
				myServerID = new ServerChatID(serverID);
			}
		}
		if (_connectTask == null) {
			_connectTask = new ChatConnectTask(MainActivity.this, PreferenceManager.getDefaultSharedPreferences(this));
			_connectTask.connect(this);
			if (_connectTask.isPending()) {
				_connectTask.execute();
			}
		}
	}

	/**
	 * Changes the communication method from "XMPP" to "HTTP" or vice versa.
	 * @param isNewCommHTTP true if changing communication method from "XMPP" to "HTTP"; false otherwise
	 */
	public void changeCommunicationMethod(boolean isNewCommHTTP) {
		if (chatConnection != null && chatConnection.isAuthenticated() && isNewCommHTTP) {
			chatConnection.disconnect();
			chatConnection = null;
			isHTTPServerAlive();
			Toast.makeText(MainActivity.this, R.string.msg_switch_http, Toast.LENGTH_LONG).show();
			_commMethod = "http";
		} else if (!isNewCommHTTP && chatConnection == null) {
			_commMethod = "xmpp";
			connectChat();
		}
	}

	/**
	 * Displays the XMPP retry to connect Dialog. Sets the status bar message accordingly.
	 */
	public void showRetryConnectDialog() {
		setChatConnected(false);
		_connectTask = null;
		if (_chatRetryConnectDialog == null || !_chatRetryConnectDialog.isShowing()) {
			AlertDialog.Builder builder = new AlertDialog.Builder(this);
			builder.setTitle(R.string.warning_retry_title);
			builder.setMessage(R.string.warning_retry_message)
				.setCancelable(false)
				.setPositiveButton(R.string.btn_yes, new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int id) {
						dialog.cancel();
						connectChat();
					}
				})
				.setNegativeButton(R.string.btn_no, new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int id) {
						YIFYFragment yFragment = getYIFYFragment();
						if (yFragment != null) {
							View v = yFragment.getView();
							if (v != null) {
								if (MediaMinerMenu != null) {
									MenuItem mnuReconnect = MediaMinerMenu.findItem(R.id.action_reconnect_chat);
									if (mnuReconnect != null && !mnuReconnect.isVisible()) {
										mnuReconnect.setVisible(true);
									}
								}
								yFragment.setStatus(getString(R.string.msg_not_connected));
							}
						}
						dialog.cancel();
					}
				});
			_chatRetryConnectDialog = builder.create();
			_chatRetryConnectDialog.show();
		}
	}

	/**
	 * When we are connected to XMPP server, this method sets all the corresponding flags accordingly and attempts to
	 * fetch the first page from YIFY site.
	 * @param c XMPPConnection object
	 */
	public void setChatData(XMPPConnection c) {
		_connectTask = null;
		chatConnection = c;
		setChatConnected(true);

		Presence presence = new Presence(Presence.Type.available);
		chatConnection.sendPacket(presence);
		// Get the roster...
		myRoster = chatConnection.getRoster();
		Collection<RosterEntry> entries = myRoster.getEntries();
		if (entries != null && entries.size() > 0) {
			for (RosterEntry re : entries) {
				if (re.getName().equals(myServerID.getName())) {
					myServerID.setID(re.getUser());
				}
			}
			// Check presence for the server...
			if (myServerID.getID() != null) {
				Presence bestPresence = myRoster.getPresence(myServerID.getID());
				myServerID.setAvailable(bestPresence.isAvailable());
			}
		}
		myRoster.setSubscriptionMode(Roster.SubscriptionMode.manual);
		myRoster.addRosterListener(new RosterListener() {
			@Override
			public void entriesAdded(Collection<String> strings) {
			}

			@Override
			public void entriesUpdated(Collection<String> strings) {
			}

			@Override
			public void entriesDeleted(Collection<String> strings) {
			}

			@Override
			public void presenceChanged(Presence presence) {
				if (presence.getFrom().equals(myServerID.getID())) {
					myServerID.setAvailable(presence.isAvailable());
				}
			}
		});
		// Hide the reconnect menu...
		if (MediaMinerMenu != null) {
			MenuItem mnuReconnect = MediaMinerMenu.findItem(R.id.action_reconnect_chat);
			if (mnuReconnect != null && mnuReconnect.isVisible()) {
				mnuReconnect.setVisible(false);
			}
		}
		// Ask server for default YIFY list..
		YIFYFragment yifyFragment = getYIFYFragment();
		if (yifyFragment != null && yifyFragment.isResponseNull()) {
			getYifyFirstPage();
		}
	}

	/**
	 * Setter method for _httpRESTStatus property.
	 * @param value true or false
	 */
	public void setHttpRestStatus(boolean value) {
		_httpRESTStatus = value;
	}

	/**
	 * Applies the filter settings and fetch the first page from YIFY site, updating the Fragment accordingly.
	 */
	public void getYifyFirstPage() {
		_yifyPage = 1;
		String filter = "0";
		switch (_filterQuality) {
			case "720p":
				filter = "1";
				break;
			case "1080p":
				filter = "2";
				break;
			case "3D":
				filter = "3";
				break;
		}
		YIFYFragment yFragment = getYIFYFragment();
		if (yFragment != null) {
			yFragment.clearMovieList();
		}
		sendMessageOut(EWebServiceCommandType.CMD_YIFY_LIST, String.valueOf(_yifyPage), filter, _filterRating);
	}

	/**
	 * Applies the filter settings and fetch subsequent page from YIFY site, updating the Fragment accordingly.
	 */
	public void getYifyNextPage() {
		_yifyPage++;
		String filter = "0";
		switch (_filterQuality) {
			case "720p":
				filter = "1";
				break;
			case "1080p":
				filter = "2";
				break;
			case "3D":
				filter = "3";
				break;
		}
		sendMessageOut(EWebServiceCommandType.CMD_YIFY_LIST, String.valueOf(_yifyPage), filter, _filterRating);
		YIFYFragment yFragment = getYIFYFragment();
		if (yFragment != null) {
			yFragment.setStatus(getString(R.string.msg_query_server));
		}
	}

	/**
	 * Resets the TPB page count (TPB page starts with 0).
	 */
	public void resetTpbPage() {
		_tpbPage = -1;
	}

	/**
	 * Getter method for _tpbPage property.
	 * @return current page
	 */
	public int getCurrentTpbPage() {
		return _tpbPage;
	}

	/**
	 * Applies the filter settings and fetch the next page of TPB search results.
	 */
	public void getTpbNextPage() {
		disableSettingsMenu();
		_tpbPage++;
		CorsairFragment corsairFragment = getCorsairFragment();
		if (corsairFragment != null) {
			String searchTerm = corsairFragment.getSearchTerm();
			String categories = corsairFragment.getSearchCategory();
			sendMessageOut(EWebServiceCommandType.CMD_TPB_SEARCH, searchTerm, String.valueOf(_tpbPage), categories);
			corsairFragment.setStatus(getString(R.string.msg_query_server));
		} else {
			enableSettingsMenu();
		}
	}

	/**
	 * Transfer the call to URLFragment object.
	 * @param view not used
	 */
	public void pasteUrl(View view) {
		ClipboardManager cm = (ClipboardManager)getSystemService(CLIPBOARD_SERVICE);
		getURLFragment().pasteUrl(cm);
	}

	/**
	 * Transfer the call to URLFragment object.
	 * @param view not used
	 */
	public void downloadUrl(View view) {
		getURLFragment().downloadUrl();
	}

	/**
	 * Lock the screen orientation into PORTRAIT mode.
	 */
	private void lockOrientation() {
		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
	}

	/**
	 * Called when a device configuration is changing.
	 * @param newConfig not used
	 */
	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_NOSENSOR);
	}

	/**
	 * Setter method for _commMethod property.
	 * @param value "http" or "xmpp"
	 */
	public void setCommunicationMethod(String value) {
		_commMethod = value;
	}

	/**
	 * Sends a message of particular type to the daemon.
	 *
	 * @param message message type
	 * @param params parameters to send to the daemon
	 * @see net.fluxo.blackpearl.EWebServiceCommandType
	 */
	public void sendMessageOut(EWebServiceCommandType message, String... params) {
		String prefComm = PreferenceManager.getDefaultSharedPreferences(MainActivity.this).getString("freebooter_pref_comm", "HTTP/Web");
		try {
			switch (message) {
				case CMD_YIFY_DETAILS:
					if (prefComm.equals("XMPP Chat")) {
						sendXMPPChatMessage("DD YIFY DETAILS " + params[0]);
					} else {
						List<String> l = new ArrayList<>();
						l.add(params[0]);
						sendWebMessage(EWebServiceCommandType.CMD_YIFY_DETAILS, l);
					}
					break;
				case CMD_YIFY_LIST:
					// page, quality, rating
					if (prefComm.equals("XMPP Chat")) {
						sendXMPPChatMessage("DD YIFY LIST " + params[0] + " " + params[1] + " " + params[2]);
					} else {
						List<String> l = new ArrayList<>();
						l.add(params[0]);
						l.add(params[1]);
						l.add(params[2]);
						sendWebMessage(EWebServiceCommandType.CMD_YIFY_LIST, l);
					}
					break;
				case CMD_YIFY_SEARCH:
					YIFYFragment yFragment = getYIFYFragment();
					if (yFragment != null) {
						yFragment.setStatus(getString(R.string.msg_searching));
					}
					if (prefComm.equals("XMPP Chat")) {
						String encoded = (new URLCodec()).encode(params[0]);
						sendXMPPChatMessage("DD YIFY SEARCH ST=\"" + encoded + "\"");
					} else {
						List<String> l = new ArrayList<>();
						l.add(params[0]);
						sendWebMessage(message, l);
					}
					break;
				case CMD_ADD_TORRENT:
					if (prefComm.equals("XMPP Chat")) {
						sendXMPPChatMessage("DD ADD_TORRENT " + params[0] + " " + params[1]);
					} else {
						List<String> l = new ArrayList<>();
						l.add(params[0]);
						l.add(params[1]);
						sendWebMessage(EWebServiceCommandType.CMD_ADD_TORRENT, l);
					}
					break;
				case CMD_ADD_HTTP:
					if (prefComm.equals("XMPP Chat")) {
						sendXMPPChatMessage("DD ADD_URI " + params[0] + " " + params[1]);
					} else {
						List<String> l = new ArrayList<>();
						l.add(params[0]);
						l.add(params[1]);
						sendWebMessage(EWebServiceCommandType.CMD_ADD_HTTP, l);
					}
					break;
				case CMD_ADD_HTTP_C:
					if (prefComm.equals("XMPP Chat")) {
						sendXMPPChatMessage("DD ADD_URI_C");
					} else {
						List<String> l = new ArrayList<>();
						l.add(params[0]);
						l.add(params[1]);
						l.add(params[2]);
						l.add(params[3]);
						sendWebMessage(EWebServiceCommandType.CMD_ADD_HTTP_C, l);
					}
					break;
				case CMD_TPB_SEARCH:
					if (prefComm.equals("XMPP Chat")) {
						sendXMPPChatMessage("DD TPB ST=\"" + (new URLCodec()).encode(params[0]) + "\" PG=" + params[1] + " CAT=" + params[2]);
					} else {
						List<String> l = new ArrayList<>();
						l.add((new URLCodec()).encode(params[0]));
						l.add(params[1]);
						l.add(params[2]);
						sendWebMessage(EWebServiceCommandType.CMD_TPB_SEARCH, l);
					}
					break;
				case CMD_TPB_DETAILS:
					if (prefComm.equals("XMPP Chat")) {
						sendXMPPChatMessage("DD TPBDETAILS " + URLEncoder.encode(params[0], "UTF-8"));
					} else {
						List<String> l = new ArrayList<>();
						l.add(URLEncoder.encode(params[0], "UTF-8"));
						sendWebMessage(EWebServiceCommandType.CMD_TPB_DETAILS, l);
					}
					break;
				case CMD_STATUS:
					if (prefComm.equals("XMPP Chat")) {
						sendXMPPChatMessage("DD STATUS " + params[0]);
					} else {
						List<String> l = new ArrayList<>();
						l.add(params[0]);
						sendWebMessage(EWebServiceCommandType.CMD_STATUS, l);
					}
					break;
			}
		} catch (Exception e) {
			Log.e("FREEBOOTER", "Error sending message to server!");
			Log.e("FREEBOOTER", e.getMessage());
		}
	}

	/**
	 * Sends a message of particular type to the daemon, via XMPP.
	 *
	 * @param message chat message to be sent to the daemon
	 */
	public void sendXMPPChatMessage(String message) {
		if (chatConnection != null && chatConnection.isAuthenticated() && myServerID != null && myServerID.isAvailable()) {
			if (myServerChat == null) {
				myServerChat = chatConnection.getChatManager().createChat(myServerID.getID(), new XMPPMessageListener());
			}
			try {
				myServerChat.sendMessage(message);
			} catch (XMPPException xmppe) {
				Log.e("FREEBOOTER", "Sending chat message to " + myServerID.getID() + " failed!");
				Log.e("FREEBOOTER", xmppe.getMessage());
			}
		} else {
			Toast.makeText(MainActivity.this, R.string.warning_no_server_chat, Toast.LENGTH_LONG).show();
			YIFYFragment yFragment = getYIFYFragment();
			if (yFragment != null) {
				yFragment.setStatus(getString(R.string.msg_chat_not_connected));
			}
			Utils.vibrate(MainActivity.this);
		}
	}

	/**
	 * Checks whether our daemon is alive, via a PING request.
	 *
	 * @return true if daemon is alive; false otherwise
	 */
	public boolean isHTTPServerAlive() {
		sendWebMessage(EWebServiceCommandType.CMD_PING, null);
		return _httpRESTStatus;
	}

	/**
	 * Send a message of particular type to the daemon, via HTTP REST.
	 * @param ctype message type
	 * @param params List of String parameters to be sent to the daemon
	 * @see net.fluxo.blackpearl.EWebServiceCommandType
	 */
	public void sendWebMessage(EWebServiceCommandType ctype, List<String> params) {
		String server = PreferenceManager.getDefaultSharedPreferences(MainActivity.this).getString("freebooter_pref_web_address", "");
		if (server.endsWith("/")) {
			server = server.substring(0, server.length());
		}
		if (server.length() > 0) {
			try {
				switch (ctype) {
					case CMD_PING:
						RestConnectTask restConnect = new RestConnectTask(MainActivity.this, server + EWebServiceCommandType.CMD_PING.getCommand(),
							EWebServiceCommandType.CMD_PING);
						restConnect.connect(this);
						if (restConnect.isPending()) {
							restConnect.execute();
						}
						break;
					case CMD_YIFY_LIST:
						if (params.size() >= 3) {
							String request = EWebServiceCommandType.CMD_YIFY_LIST.getCommand();
							request = request.replace("{page}", params.get(0)).replace("{quality}", params.get(1)).replace("{rating}", params.get(2));
							restConnect = new RestConnectTask(MainActivity.this, server + request, EWebServiceCommandType.CMD_YIFY_LIST);
							restConnect.connect(this);
							if (restConnect.isPending()) {
								restConnect.execute();
							}
						}
						break;
					case CMD_YIFY_DETAILS:
						if (params.size() >= 1) {
							String request = EWebServiceCommandType.CMD_YIFY_DETAILS.getCommand();
							request = request.replace("{id}", params.get(0));
							restConnect = new RestConnectTask(MainActivity.this, server + request, EWebServiceCommandType.CMD_YIFY_DETAILS);
							restConnect.connect(this);
							if (restConnect.isPending()) {
								restConnect.execute();
							}
						}
						break;
					case CMD_YIFY_SEARCH:
						if (params.size() >= 1) {
							String request = EWebServiceCommandType.CMD_YIFY_SEARCH.getCommand();
							request = request.replace("{st}", (new URLCodec()).encode(params.get(0)));
							restConnect = new RestConnectTask(MainActivity.this, server + request,
								EWebServiceCommandType.CMD_YIFY_SEARCH);
							restConnect.connect(this);
							if (restConnect.isPending()) {
								restConnect.execute();
							}
						}
						break;
					case CMD_STATUS:
						if (params.size() >= 1) {
							String request = EWebServiceCommandType.CMD_STATUS.getCommand();
							request = request.replace("{id}", params.get(0));
							restConnect = new RestConnectTask(MainActivity.this, server + request,
								EWebServiceCommandType.CMD_STATUS);
							restConnect.connect(this);
							if (restConnect.isPending()) {
								restConnect.execute();
							}
						}
						break;
					case CMD_ADD_TORRENT:
						if (params.size() >= 2) {
							String request = EWebServiceCommandType.CMD_ADD_TORRENT.getCommand();
							request = request.replace("{uri}", (URLEncoder.encode(params.get(0), "UTF-8")))
								.replace("{owner}", params.get(1));
							restConnect = new RestConnectTask(MainActivity.this, server + request, EWebServiceCommandType.CMD_ADD_TORRENT);
							restConnect.connect(this);
							if (restConnect.isPending()) {
								restConnect.execute();
							}
						}
						break;
					case CMD_TPB_SEARCH:
						if (params.size() >= 3) {
							String request = EWebServiceCommandType.CMD_TPB_SEARCH.getCommand();
							request = request.replace("{st}", params.get(0))
								.replace("{page}", params.get(1))
								.replace("{cat}", params.get(2));
							restConnect = new RestConnectTask(MainActivity.this, server + request, EWebServiceCommandType.CMD_TPB_SEARCH);
							restConnect.connect(this);
							if (restConnect.isPending()) {
								restConnect.execute();
							}
						}
						break;
					case CMD_TPB_DETAILS:
						if (params.size() >= 1) {
							String request = EWebServiceCommandType.CMD_TPB_DETAILS.getCommand();
							request = request.replace("{url}", params.get(0));
							restConnect = new RestConnectTask(MainActivity.this, server + request, EWebServiceCommandType.CMD_TPB_DETAILS);
							restConnect.connect(this);
							if (restConnect.isPending()) {
								restConnect.execute();
							}
						}
						break;
					case CMD_ADD_HTTP:
						if (params.size() >= 2) {
							String request = EWebServiceCommandType.CMD_ADD_HTTP.getCommand();
							request = request.replace("{owner}", params.get(1)).replace("{uri}", (URLEncoder.encode(params.get(0), "UTF-8")));
							restConnect = new RestConnectTask(MainActivity.this, server + request, EWebServiceCommandType.CMD_ADD_HTTP);
							restConnect.connect(this);
							if (restConnect.isPending()) {
								restConnect.execute();
							}
						}
						break;
					case CMD_ADD_HTTP_C:
						if (params.size() >= 4) {
							String request = EWebServiceCommandType.CMD_ADD_HTTP_C.getCommand();
							request = request.replace("{owner}", params.get(1)).replace("{uri}", (URLEncoder.encode(params.get(0), "UTF-8")))
								.replace("{username}", params.get(2)).replace("{password}", params.get(3));
							restConnect = new RestConnectTask(MainActivity.this, server + request, EWebServiceCommandType.CMD_ADD_HTTP_C);
							restConnect.connect(this);
							if (restConnect.isPending()) {
								restConnect.execute();
							}
						}
						break;
				}
			} catch (Exception e) {
				Toast.makeText(MainActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
				Log.e("FREEBOOTER", "Error querying REST server!");
				Log.e("FREEBOOTER", "" + e.getMessage());
				e.printStackTrace();
			}
		}
	}

	/**
	 * Sets the filter value for YIFY search.
	 * @param type value type, either QUALITY or RATING
	 * @param value filter value
	 */
	@Override
	public void setFilterValue(int type, String value) {
		switch (type) {
			case SpinnerItemSelectedListener.T_QUALITY:
				_filterQuality = value;
				break;
			case SpinnerItemSelectedListener.T_RATING:
				_filterRating = value;
				break;
		}
	}

	/**
	 * This class implements XMPP message listener and handling.
	 */
	class XMPPMessageListener implements MessageListener {
		/**
		 * Process the received message from the daemon and update app parts accordingly.
		 * @param chat  XMPP Chat object
		 * @param message XMPP Message object
		 */
		public void processMessage(Chat chat, Message message) {
			if (message != null && message.getBody() != null) {
				processMessage(message.getBody());
			}
		}

		/**
		 * Process the received message from the daemon and update app parts accordingly.
		 * @param msg a String object containing the message received from the daemon
		 */
		public void processMessage(String msg) {
			Pattern tpbXMPPPattern = Pattern.compile("^[\\d]+\\/[\\d]+[\\:]{3}");
			Matcher matcher = tpbXMPPPattern.matcher(msg);
			if (msg.startsWith("OK") || msg.startsWith("ERR") || msg.startsWith("SYNTAX")) {
				Toast.makeText(MainActivity.this, "Server: " + msg, Toast.LENGTH_LONG).show();
				Utils.vibrate(MainActivity.this);
			} else if (msg.startsWith("{\"MovieCount")) {        // YIFY LIST
				YIFYResponse response = Utils.processJSON(msg);
				YIFYFragment yFragment = getYIFYFragment();
				if (yFragment != null) {
					yFragment.refreshContent(response);
				}
			} else if (msg.startsWith("{\"MovieID")) {           // YIFY Movie Details
				YIFYObjectDetails response = Utils.processDetailsJSON(msg);
				YIFYDetails yDetails = new YIFYDetails(response);
				FragmentTransaction fm = getFragmentManager().beginTransaction();
				Fragment previousFragment = getFragmentManager().findFragmentByTag("YIFYDetails");
				if (previousFragment != null) {
					fm.remove(previousFragment);
				}
				dismissProgressDialog();
				yDetails.show(fm, "YIFYDetails");
			} else if (msg.startsWith("{\"SearchResult\"")) {
				try {
					JSONObject json = new JSONObject(msg);
					if (json.getString("SearchResult").equals("YIFY")) {
						YIFYResponse response = Utils.processJSON(msg);
						YIFYFragment yFragment = getYIFYFragment();
						if (yFragment != null) {
							yFragment.displaySearchResult(response);
						}
					} else if (json.getString("SearchResult").equals("TPB")) {
						TPBResponse response = Utils.processTPBDetailsJSON(msg);
						CorsairFragment cFragment = getCorsairFragment();
						if (cFragment != null) {
							cFragment.displaySearchResult(response);
						}
					}
				} catch (JSONException jse) {
					Log.e("FREEBOOTER", "Error trying to parse search result");
					Log.e("FREEBOOTER", jse.getMessage());
				}
			} else if (msg.contains("TPB_DETAILS")) {
				TPBObjectDetails t = Utils.processTPBInfoJSON(msg, MainActivity.this);
				TPBDetails tDetails = new TPBDetails(t);
				FragmentTransaction fm = getFragmentManager().beginTransaction();
				dismissProgressDialog();
				tDetails.show(fm, "Corsair");
			} else if (msg.contains("Object") && msg.contains("DownloadProgress")) {
				Utils.processAndUpdateDownloadProgressData(MainActivity.this, msg);
			} else if (matcher.lookingAt()) {
				// This is the TPB Search Results from XMPP Chat...
				TPBResponse procResponse = Utils.processTPBXMPPResult(msg);
				if (procResponse != null) {
					CorsairFragment cFragment = getCorsairFragment();
					if (cFragment != null) {
						cFragment.displaySearchResult(procResponse);
					}
				}
			}
		}
	}

}
