/*
 * YCustomViewAdapter.java
 *
 * Copyright (c) 2014 Ronald Kurniawan. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 */
package net.fluxo.blackpearl;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.koushikdutta.urlimageviewhelper.UrlImageViewHelper;
import net.fluxo.blackpearl.dto.YIFYObject;

import java.util.List;

/**
 * BaseAdapter class for YIFY Fragment that governs the items displayed within the Fragment.
 *
 * @author Ronald Kurniawan (viper)
 * @version 0.2.6, 28/03/2014
 */
public class YCustomViewAdapter extends BaseAdapter {

	private Context _ctx;
	private static List<YIFYObject> _items = null;
	private Typeface _tf = null;

	public YCustomViewAdapter(Context context, Typeface font, List<YIFYObject> items) {
		_ctx = context;
		_items = items;
		_tf = font;
	}

	/**
	 * Returns the number of items contained in the Adapter.
	 * @return number of items in the Adapter
	 */
	@Override
	public int getCount() {
		return _items.size();
	}

	/**
	 * Get the item i from the List.
	 * @param i index of the item to be fetched
	 * @return Object
	 */
	@Override
	public Object getItem(int i) {
		return null;
	}

	/**
	 * Get the item ID.
	 * @param i index of the item to be fetched
	 * @return item ID
	 */
	@Override
	public long getItemId(int i) {
		return 0;
	}

	/**
	 * Returns the View for a single item in the YIFY fragment.
	 *
	 * @param i index of the item to be displayed
	 * @param view a View object
	 * @param viewGroup a ViewGroup object
	 * @return a View object for a single item
	 */
	@Override
	public View getView(final int i, View view, ViewGroup viewGroup) {
		LayoutInflater inflater =  (LayoutInflater)_ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		@SuppressLint({"ViewHolder", "InflateParams"}) View gridView = inflater.inflate(R.layout.yifi_cell, null);
		if (gridView != null && _items.get(i) != null) {
			ImageView ivCover = (ImageView) gridView.findViewById(R.id.mv_cover);
			Drawable dummyCover = _ctx.getResources().getDrawable(R.drawable.dummy_cover);
			if (_items != null && _items.get(i) != null && _items.get(i).getCoverURL().length() > 0) {
				UrlImageViewHelper.setUrlDrawable(ivCover, _items.get(i).getCoverURL(), dummyCover);
			} else {
				ivCover.setImageDrawable(dummyCover);
			}
			TextView tvCover = (TextView) gridView.findViewById(R.id.tv_cover);
			if (_tf != null) {
				tvCover.setTypeface(_tf);
			}
			tvCover.setTextColor(_ctx.getResources().getColor(R.color.fluxo_orange));
			StringBuilder sb = new StringBuilder();
			if (_items != null && _items.get(i) != null) {
				sb.append(_items.get(i).getMovieTitle()).append(" (").append(_items.get(i).getMovieYear())
					.append(")\n").append(_items.get(i).getMovieQuality()).append("\n")
					.append("Size: ").append(_items.get(i).getMovieSize()).append("\n")
					.append("Seeders/Peers: ").append(_items.get(i).getTorrentSeeds()).append("/")
					.append(_items.get(i).getTorrentPeers());
				tvCover.setText(sb.toString());
			}
			//notifyDataSetChanged();
		}
		return gridView;
	}
}
