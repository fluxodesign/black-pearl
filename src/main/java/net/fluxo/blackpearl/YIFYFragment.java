/*
 * YIFYFragment.java
 *
 * Copyright (c) 2014 Ronald Kurniawan. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 */
package net.fluxo.blackpearl;

import android.app.Fragment;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.TextView;
import android.widget.Toast;
import net.fluxo.blackpearl.dto.YIFYObject;
import net.fluxo.blackpearl.dto.YIFYResponse;

/**
 * An implementation of Fragment that displays the items from YIFY server, from the latest to
 * oldest.
 *
 * @author Ronald Kurniawan (viper)
 * @version 0.2.6, 29/03/2014
 */
public class YIFYFragment extends Fragment {

	private YIFYResponse _yr;
	private long _totalMovies = -1;
	private int _currentPage = 0;
	private boolean _isSearchResult = false;
	private View _fragmentView = null;

	/**
	 * Called when the Fragment is created.
	 *
	 * @param savedInstanceState a Bundle object
	 */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setRetainInstance(true);
	}

	/**
	 * Inflates the layout for this Fragment and populates the values specified in the layout. This
	 * method also specifies the listener for scrolling events (when there are more items to be
	 * loaded). This method will also broadcast an Intent of the type "FRAGMENT_CHANGE".
	 *
	 * @param inflater a LayoutInflater object
	 * @param container a ViewGroup object
	 * @param savedInstanceState a Bundle object
	 * @return a View object
	 */
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View v;
		if (getActivity().getResources().getBoolean(R.bool.isTablet)) {
			v = inflater.inflate(R.layout.yify_fragment_tablet, container, false);
		} else {
			v = inflater.inflate(R.layout.yify_fragment, container, false);
		}
		_fragmentView = v;
		if (v != null) {
			GridView gv = (GridView)v.findViewById(R.id.gvMain);
			final TextView tvStatus = (TextView)v.findViewById(R.id.tv_status);
			tvStatus.setTypeface(Utils.getDefaultFont(getActivity()));
			if (_yr != null) {
				gv.setAdapter(new YCustomViewAdapter(getActivity(), Utils.getDefaultFont(getActivity()), _yr.getMovies()));
				gv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
					@Override
					public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
						if (getActivity() != null) {
							((MainActivity)getActivity()).setProgressDialog(getString(R.string.msg_query_server_details));
							((MainActivity)getActivity()).sendMessageOut(EWebServiceCommandType.CMD_YIFY_DETAILS, _yr.getMovies().get(i).getMovieID());
						}
					}
				});
				gv.setOnScrollListener(new ScrollListener() {
					@Override
					public void onLoadMore(int page, int totalItemsCount) {
						if (getActivity() != null) {
							if (hasMorePages()) {
								((MainActivity) getActivity()).getYifyNextPage();
							}
						}
					}
				});
				tvStatus.setText(_yr.getMovies().size() + " " + getString(R.string.msg_items));
			} else {
				tvStatus.setText(R.string.msg_query_server);
			}
		}
		if (getActivity() != null) {
			Intent intent = new Intent("net.fluxo.mediaminer.FRAGMENT_CHANGE").putExtra("new_fragment", "2");
			getActivity().sendBroadcast(intent);
		}
		return v;
	}

	/**
	 * Called when the Fragment is resumed.
	 */
	@Override
	public void onResume() {
		super.onResume();

		if ((_yr == null || _yr.getMovies().size() == 0) && getActivity() != null) {
			MainActivity mActivity = (MainActivity)getActivity();
			if (Utils.isConnectedToInternet(getActivity()) && !mActivity.isChatConnected()) {
				Toast.makeText(mActivity, R.string.msg_query_server, Toast.LENGTH_LONG).show();
				SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(mActivity);
				if (pref.getString("freebooter_pref_comm", "").equals("XMPP Chat")) {
					mActivity.connectChat();
					mActivity.setCommunicationMethod("xmpp");
				} else {
					mActivity.isHTTPServerAlive();
					mActivity.getYifyFirstPage();
					mActivity.setCommunicationMethod("http");
				}
			} else if (!Utils.isConnectedToInternet(getActivity())) {
				Toast.makeText(mActivity, getString(R.string.warning_no_internet), Toast.LENGTH_LONG).show();
				setStatus(getString(R.string.msg_not_connected));
				Utils.vibrate(mActivity);
			}
		}
	}

	/**
	 * Checks whether the YIFYResponse object is null.
	 * @return true if object is null; false otherwise
	 */
	public boolean isResponseNull() {
		return (_yr == null);
	}

	/**
	 * Refreshes the items displayed on this Fragment.
	 * @param response a YIFYResponse object
	 */
	public void refreshContent(YIFYResponse response) {
		if (getActivity() != null) {
			MainActivity ma = (MainActivity)getActivity();
			if (!ma.appRunning) {
				return;
			}
		}
		_currentPage++;
		_totalMovies = response.getMovieCount();
		addItemsToEndOfList(response);
		if (getFragmentManager() != null) {
			getFragmentManager().beginTransaction().detach(this).attach(this).commit();
		}
		if (getActivity().getActionBar() != null) {
			getActivity().getActionBar().setSelectedNavigationItem(1);
		}
	}

	/**
	 * Displays the search results of the movie title. A blank query will reset the display
	 * to all items.
	 * @param response a YIFYResponse object
	 */
	public void displaySearchResult(YIFYResponse response) {
		_isSearchResult = true;
		_currentPage = 1;
		_totalMovies = response.getMovieCount();
		clearMovieList();
		addItemsToEndOfList(response);
		if (getFragmentManager() != null) {
			getFragmentManager().beginTransaction().detach(this).attach(this).commit();
		}
		if (response.getMovies().size() == 0) {
			if (getActivity() != null) {
				Toast.makeText(getActivity(), getString(R.string.msg_search_found_nothing), Toast.LENGTH_LONG).show();
			}
		}
	}

	/**
	 * Add response object to the end of the list.
	 * @param y a YIFYResponse object
	 */
	private void addItemsToEndOfList(YIFYResponse y) {
		if (_yr == null) {
			_yr = y;
		} else {
			for (YIFYObject o : y.getMovies()) {
				_yr.getMovies().add(o);
			}
		}
	}

	/**
	 * Clears the list where items are stored.
	 */
	public void clearMovieList() {
		_isSearchResult = false;
		if (_yr != null) {
			_yr.getMovies().clear();
			_totalMovies = -1;
			_currentPage = 0;
		}
	}

	/**
	 * Checks whether there are more pages to be loaded.
	 * @return true if there are more results to be loaded; false otherwise
	 */
	private boolean hasMorePages() {
		if (_totalMovies > 0 && !_isSearchResult) {
			int MOVIES_PER_PAGE = 15;
			int totalPages = (int)_totalMovies / MOVIES_PER_PAGE;
			if (_totalMovies % MOVIES_PER_PAGE > 0) {
				totalPages += 1;
			}
			if (_currentPage < totalPages) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Sets the string to be displayed on the status bar at the bottom of the screen.
	 * @param value status string to be displayed
	 */
	public void setStatus(String value) {
		if (_fragmentView != null) {
			TextView tvStatus = (TextView)_fragmentView.findViewById(R.id.tv_status);
			tvStatus.setText(value);
		}
	}
}
