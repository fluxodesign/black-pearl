/*
 * CorsairFragment.java
 *
 * Copyright (c) 2014 Ronald Kurniawan. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 */
package net.fluxo.blackpearl;

import android.app.AlertDialog;
import android.app.Fragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.TextView;
import net.fluxo.blackpearl.dto.TPBObjectDetails;
import net.fluxo.blackpearl.dto.TPBResponse;

/**
 * An implementation of a Fragment that displays the search results from TPB site.
 *
 * @author Ronald Kurniawan (viper)
 * @version 0.2.6, 30/03/2014
 */
public class CorsairFragment extends Fragment {

	private View _fragmentView = null;
	private long _totalItems = -1;
	private TPBResponse _tpbr = null;
	private String _searchTerm = "";
	private String _searchCategory = "0";
	private boolean _isNew = true;

	/**
	 * Getter method for _searchTerm property.
	 * @return the search term input by user
	 */
	public String getSearchTerm() {
		return _searchTerm;
	}

	/**
	 * Returns the search categories selected by user.
	 * @return search categories
	 */
	public String getSearchCategory() {
		return _searchCategory;
	}

	/**
	 * Is this the first time this Fragment is displayed?
	 * @return true if this is first time the Fragment is displayed; false otherwise
	 */
	public boolean isFreshLook() {
		return _isNew;
	}

	/**
	 * Setter method for _searchCategory property.
	 * @param value search categories selected by user
	 */
	public void setSearchCategory(String value) {
		_searchCategory = value;
	}

	/**
	 * Returns the TPB item details object based on the URL supplied.
	 * @param url URL to item details
	 * @return TPBObjectDetails object
	 */
	public TPBObjectDetails findObjectByDetailsURL(String url) {
		if (_tpbr != null && _tpbr.getItems().size() > 0) {
			for (TPBObjectDetails t : _tpbr.getItems()) {
				if (t.getDetailsURL().contains(url)) {
					return t;
				}
			}
		}
		return null;
	}

	/**
	 * Called when this Fragment was created.
	 * @param savedInstanceState a Bundle object
	 */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setRetainInstance(true);
	}

	/**
	 * Inflates the layout for the Corsair fragment and populates the values specified within the layout.
	 * This method also specifies the click listeners for each item and the listener for scrolling
	 * event (when there are more items to be loaded). This method will also broadcast an Intent of
	 * the type "FRAGMENT_CHANGE".
	 *
	 * @param inflater a LayoutInflater object
	 * @param container a ViewGroup object
	 * @param savedInstanceState a Bundle object
	 * @return a View object
	 */
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.tpb_fragment, container, false);
		_fragmentView = v;
		if (v != null) {
			GridView gv = (GridView)v.findViewById(R.id.gv_tpb);
			final TextView tvTpbStatus = (TextView)v.findViewById(R.id.tv_tpb_status);
			tvTpbStatus.setTypeface(Utils.getDefaultFont(getActivity()));
			if (_tpbr == null || _tpbr.getItems().size() == 0) {
				setStatus(getString(R.string.msg_no_search));
			}else if (_tpbr.getItems().size() > 0) {
				gv.setAdapter(new TCustomViewAdapter(getActivity(), Utils.getDefaultFont(getActivity()), _tpbr.getItems()));
				gv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
					@Override
					public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
						if (getActivity() != null) {
							((MainActivity) getActivity()).setProgressDialog(getString(R.string.msg_query_server_details));
							((MainActivity) getActivity()).sendMessageOut(EWebServiceCommandType.CMD_TPB_DETAILS, _tpbr.getItems().get(i).getDetailsURL());
						}
					}
				});
				gv.setOnScrollListener(new ScrollListener() {
					@Override
					public void onLoadMore(int page, int totalItemsCount) {
						if (getActivity() != null) {
							if (hasMorePages()) {
								((MainActivity)getActivity()).getTpbNextPage();
							}
						}
					}
				});
				setStatus(_tpbr.getItems().size() + "/" + _totalItems);
			} else {
				setStatus(getString(R.string.msg_query_server));
			}
		}
		if (getActivity() != null) {
			Intent intent = new Intent("net.fluxo.mediaminer.FRAGMENT_CHANGE").putExtra("new_fragment", "3");
			getActivity().sendBroadcast(intent);
		}
		return v;
	}

	@Override
	public void onResume() {
		super.onResume();
	}

	/**
	 * Displays an AlertDialog when Fragment is displayed for the first time, asking the user
	 * to set the categories for searches.
	 */
	public void showAlertNoSearch() {
		_isNew = false;
		if (getActivity() != null) {
			AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
			builder.setMessage(R.string.warning_tpb_no_search).setCancelable(false)
				.setPositiveButton(R.string.btn_ok, new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialogInterface, int i) {
						dialogInterface.cancel();
					}
				});
			AlertDialog dialog = builder.create();
			dialog.show();
		}
	}

	/**
	 * Adds response objects to the end of the List
	 * @param t a TPBResponse object
	 */
	private void addItemsToEndOfList(TPBResponse t) {
		if (_tpbr == null) {
			_tpbr = t;
		} else {
			for (TPBObjectDetails tpbo : t.getItems()) {
				_tpbr.getItems().add(tpbo);
			}
		}
	}

	/**
	 * Displays the AlertDialog for user to input the search term.
	 */
	public void showSearchDialog() {
		if (getActivity() != null) {
			LayoutInflater inflater = LayoutInflater.from(getActivity());
			View promptView = inflater.inflate(R.layout.yify_search, null);
			AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
			builder.setView(promptView);
			if (promptView != null) {
				final EditText userInput = (EditText)promptView.findViewById(R.id.et_search_term);
				builder.setCancelable(true).setPositiveButton(R.string.action_search, new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int id) {
						if (userInput.getText() != null && userInput.getText().length() > 0) {
							// we need to reset the page counter to -1 on MainActivity
							_searchTerm = userInput.getText().toString();
							((MainActivity)getActivity()).resetTpbPage();
							((MainActivity)getActivity()).getTpbNextPage();
						}
					}
				}).setNegativeButton(R.string.btn_cancel, new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialogInterface, int i) {
						dialogInterface.cancel();
					}
				});
			}
			AlertDialog theDialog = builder.create();
			theDialog.show();
		}
	}

	/**
	 * Displays the latest search results on the Fragment and makes the fragment active when
	 * items are displayed.
	 * @param response a TPBResponse object that contains the search results
	 */
	public void displaySearchResult(TPBResponse response) {
		if (getActivity() != null) {
			MainActivity ma = (MainActivity)getActivity();
			if (!ma.appRunning) {
				return;
			}
			if (getCurrentPage() == 0) {
				clearItemList();
			}
			addItemsToEndOfList(response);
			_totalItems = response.getItemCount();
			if (getFragmentManager() != null) {
				getFragmentManager().beginTransaction().detach(this).attach(this).commit();
			}
			if (getActivity().getActionBar() != null) {
				getActivity().getActionBar().setSelectedNavigationItem(2);
			}
		}
	}

	/**
	 * Clears the list where search results are stored.
	 */
	public void clearItemList() {
		if (_tpbr != null) {
			_tpbr.getItems().clear();
			_totalItems = -1;
		}
	}

	/**
	 * Checks whether there are more pages to be loaded, based on the results returned from server.
	 * @return true if there are more pages to be loaded; false otherwise
	 */
	private boolean hasMorePages() {
		if (_totalItems > 0) {
			int ITEMS_PER_PAGE = 30;
			int totalPages = (int)_totalItems / ITEMS_PER_PAGE;
			if (_totalItems % ITEMS_PER_PAGE > 0) {
				totalPages += 1;
			}
			if (getCurrentPage() < totalPages - 1) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Returns the current page number of the search results, (0-based).
	 * @return the current page number
	 */
	private int getCurrentPage() {
		if (getActivity() != null) {
			return ((MainActivity)getActivity()).getCurrentTpbPage();
		}
		return 0;
	}

	/**
	 * Sets the string in the status bar at the bottom of the screen.
	 * @param value a String to be set into the status bar
	 */
	public void setStatus(String value) {
		if (_fragmentView != null) {
			TextView tvTpbStatus = (TextView)_fragmentView.findViewById(R.id.tv_tpb_status);
			tvTpbStatus.setText(value);
		}
	}
}
