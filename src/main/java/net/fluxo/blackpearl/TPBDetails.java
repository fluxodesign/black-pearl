/*
 * TPBDetails.java
 *
 * Copyright (c) 2014 Ronald Kurniawan. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 */
package net.fluxo.blackpearl;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;
import net.fluxo.blackpearl.dto.TPBObjectDetails;


/**
 * This is a DialogFragment that displays the details for one TPB item.
 *
 * @author Ronald Kurniawan (viper)
 * @version 0.2.6, 2/05/14
 */
public class TPBDetails extends DialogFragment {

	private TPBObjectDetails _tpbod;

	public TPBDetails(TPBObjectDetails object) {
		_tpbod = object;
	}

	/**
	 * Called when the DialogFragment is created.
	 * @param savedInstanceState a Bundle object
	 */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setStyle(DialogFragment.STYLE_NO_TITLE, android.R.style.Theme_Black_NoTitleBar);
	}

	/**
	 * Called when the Dialog is created. It inflates the layout for the Dialog and populate the values inside the Dialog.
	 * @param savedInstanceState a Bundle object
	 * @return Dialog object
	 */
	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		super.onCreateDialog(savedInstanceState);
		if (getActivity() != null) {
			LayoutInflater inflater = getActivity().getLayoutInflater();
			@SuppressLint("InflateParams") View view = inflater.inflate(R.layout.tpb_details, null);
			if (view != null) {
				Typeface tf = Utils.getDefaultFont(getActivity());
				TextView tvTitle = (TextView)view.findViewById(R.id.tv_tpb_details_title);
				tvTitle.setTypeface(tf);
				tvTitle.setText(_tpbod.getTitle());
				TextView tvInfo = (TextView)view.findViewById(R.id.tv_tpb_details_info);
				tvInfo.setTypeface(tf);
				tvInfo.setText(_tpbod.getInfo());
				TextView tvDetails = (TextView)view.findViewById(R.id.tv_tpb_details_details);
				tvDetails.setTypeface(tf);
				StringBuilder sb = new StringBuilder();
				sb.append("UPLOADED: ").append(_tpbod.getUploaded()).append("\n")
					.append("UPLOADER: ").append(_tpbod.getUploader()).append("\n")
					.append("SIZE: ").append(_tpbod.getSize()).append("\n")
					.append("CATEGORY: ").append(Utils.getTPBCategory(_tpbod.getType())).append("\n")
					.append("SEEDER(S): ").append(_tpbod.getSeeders()).append("\n")
					.append("LEECHER(S): ").append(_tpbod.getLeechers());
				tvDetails.setText(sb.toString());

				// Listeners for clicks...
				// The listener for POSITIVE button is REMOVED!
				DialogInterface.OnClickListener negativeListener = new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.cancel();
					}
				};
				return new AlertDialog.Builder(getActivity())
					.setView(view)
					.setNegativeButton(R.string.layout_cancel, negativeListener).create();
			}
		}
		return null;
	}
}
