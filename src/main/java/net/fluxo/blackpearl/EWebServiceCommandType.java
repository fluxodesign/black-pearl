/*
 * EWebServiceCommandType.java
 *
 * Copyright (c) 2014 Ronald Kurniawan. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 */
package net.fluxo.blackpearl;

/**
 * This is the Enumeration that lists the commands available on the daemon.
 *
 * @author Ronald Kurniawan (viper)
 * @version 0.2.6, 13/04/14
 */
public enum EWebServiceCommandType {

	CMD_PING(0, "/comm/rs/ws/ping"),
	CMD_YIFY_LIST(1, "/comm/rs/ws/ylist/page/{page}/quality/{quality}/rating/{rating}"),
	CMD_YIFY_DETAILS(2, "/comm/rs/ws/ydetails/{id}"),
	CMD_YIFY_SEARCH(3, "/comm/rs/ws/ysearch?st={st}"),
	CMD_STATUS(4, "/comm/rs/ws/status/{id}"),
	CMD_ADD_TORRENT(5, "/comm/rs/ws/addtorrent/{owner}/{uri}"),
	CMD_ADD_HTTP(6, "/comm/rs/ws/adduri/{owner}/{uri}"),
	CMD_ADD_HTTP_C(7, "/comm/rs/ws/adduric/{owner}/{username}/{password}/{uri}"),
	CMD_TPB_SEARCH(8, "/comm/rs/ws/tpb/{st}/{page}/{cat}"),
	CMD_TPB_DETAILS(9, "/comm/rs/ws/tpbdetails/{url}");

	protected int _type;
	private String _command;

	EWebServiceCommandType(int type, String command) {
		_type = type;
		_command = command;
	}

	public String getCommand() {
		return _command;
	}
}
