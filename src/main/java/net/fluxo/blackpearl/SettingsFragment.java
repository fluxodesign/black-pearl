/*
 * SettingsFragment.java
 *
 * Copyright (c) 2014 Ronald Kurniawan. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 */
package net.fluxo.blackpearl;

import android.os.Bundle;
import android.preference.PreferenceFragment;

/**
 * Fragment that displays the PreferenceFragment.
 *
 * @author Ronald Kurniawan (viper)
 * @version 0.2.6, 28/03/2014
 */
public class SettingsFragment extends PreferenceFragment {

	/**
	 * Called when Fragment is created.
	 * @param savedStateInstance a Bundle object
	 */
	@Override
	public void onCreate(Bundle savedStateInstance) {
		super.onCreate(savedStateInstance);
		addPreferencesFromResource(R.xml.preferences);
	}
}
