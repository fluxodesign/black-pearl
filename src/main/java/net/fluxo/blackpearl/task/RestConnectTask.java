/*
 * RestConnectTask.java
 *
 * Copyright (c) 2014 Ronald Kurniawan. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 */
package net.fluxo.blackpearl.task;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;
import android.widget.Toast;
import net.fluxo.blackpearl.*;
import net.fluxo.blackpearl.dto.TPBObjectDetails;
import net.fluxo.blackpearl.dto.TPBResponse;
import net.fluxo.blackpearl.dto.YIFYObjectDetails;
import net.fluxo.blackpearl.dto.YIFYResponse;
import net.fluxo.blackpearl.taskhandler.IgnitedAsyncTask;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;

/**
 * This is a Task that deals with sending HTTP REST requests to the daemon.
 *
 * @author Ronald Kurniawan (viper)
 * @version 0.2.6, 18/04/2014
 */
public class RestConnectTask extends IgnitedAsyncTask<MainActivity, Void, Void, String> {

	private MainActivity _parent;
	private String _request;
	private EWebServiceCommandType _type;
	private int _lastResponseStatus = -1;

	public RestConnectTask(MainActivity parent, String requestUrl, EWebServiceCommandType type) {
		super();
		_parent = parent;
		_request = requestUrl;
		_type = type;
	}

	/**
	 * Builds a HTTP Request and send it to the daemon, and waits until a HTTP response is received.
	 * @param params (not used) the parameters for your task
	 * @return a String object containing the response to our request
	 */
	@Override
	public String run(Void... params) {
		StringBuilder response = new StringBuilder();
		try {
			CustomSSLHttpClient httpClient = new CustomSSLHttpClient(_parent, _request);
			HttpGet htGet = new HttpGet(_request);
			htGet.addHeader("User-Agent", "Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:25.0) Gecko/20100101 Firefox/25.0");
			if (_type.equals(EWebServiceCommandType.CMD_STATUS) || _type.equals(EWebServiceCommandType.CMD_ADD_TORRENT) ||
				_type.equals(EWebServiceCommandType.CMD_ADD_HTTP) || _type.equals(EWebServiceCommandType.CMD_ADD_HTTP_C)) {
				SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(_parent);
				htGet.addHeader("DDUSER", pref.getString("freebooter_pref_dl_username", "username"));
				String pass = Utils.getHashValue(pref.getString("freebooter_pref_dl_password", "password"));
				htGet.addHeader("DDPWD", pass);
			}
			HttpResponse htResponse = httpClient.execute(htGet);
			_lastResponseStatus = htResponse.getStatusLine().getStatusCode();
			BufferedReader br = new BufferedReader(new InputStreamReader(htResponse.getEntity().getContent()));
			String line;
			while ((line = br.readLine()) != null) {
				response.append(line);
			}
			br.close();
		} catch (Exception e) {
			Log.e("FREEBOOTER", "Exception: " + e.getMessage());
			e.printStackTrace();
			Toast.makeText(_parent, R.string.warning_server_problem, Toast.LENGTH_LONG).show();
			_lastResponseStatus = 400;
			_parent.cancelProgressAlarm();
		}
		return response.toString();
	}

	/**
	 * This method defines what happens when the Task has finished running, based on the
	 * command type.
	 *
	 * @param context The most recent instance of the Context that executed this IgnitedAsyncTask the
	 * @param result response from the request; from run()
	 * @return true
	 */
	@Override
	public boolean onTaskCompleted(MainActivity context, String result) {
		super.onTaskCompleted(context, result);
		if (!_parent.appRunning) {
			return false;
		}
		if (result != null) {
			switch (_type) {
				case CMD_PING:
					_parent.setHttpRestStatus(result.equals("FLUXO-REST-WS"));
					break;
				case CMD_YIFY_LIST:
					YIFYResponse yresponse = Utils.processJSON(result);
					YIFYFragment yFragment = _parent.getYIFYFragment();
					if (yFragment != null) {
						yFragment.refreshContent(yresponse);
					}
					break;
				case CMD_YIFY_DETAILS:
					YIFYObjectDetails ydetails = Utils.processDetailsJSON(result);
					YIFYDetails yDetails = new YIFYDetails(ydetails);
					FragmentTransaction fm = _parent.getFragmentManager().beginTransaction();
					Fragment previousFragment = _parent.getFragmentManager().findFragmentByTag("YIFYDetails");
					if (previousFragment != null) {
						fm.remove(previousFragment);
					}
					_parent.dismissProgressDialog();
					yDetails.show(fm, "YIFYDetails");
					break;
				case CMD_YIFY_SEARCH:
					try {
						JSONObject json = new JSONObject(result);
						if (json.getString("SearchResult").equals("YIFY")) {
							yresponse = Utils.processJSON(result);
							yFragment = _parent.getYIFYFragment();
							if (yFragment != null) {
								yFragment.displaySearchResult(yresponse);
							}
						}
					} catch (JSONException jse) {
						Log.e("FREEBOOTER", "Error trying to parse search result");
						Log.e("FREEBOOTER", jse.getMessage());
					}
					break;
				case CMD_ADD_TORRENT:
				case CMD_ADD_HTTP:
				case CMD_ADD_HTTP_C:
					if (_lastResponseStatus == 200) {
						Toast.makeText(_parent, result, Toast.LENGTH_LONG).show();
						if (result.contains("OK")) {
							_parent.setProgressAlarm();
						}
					} else {
						Toast.makeText(_parent, _parent.getString(R.string.warning_rest_result) + " " + result, Toast.LENGTH_LONG).show();
						Log.e("FREEBOOTER", "Error processing status: " + result);
					}
					Utils.vibrate(_parent);
					break;
				case CMD_TPB_SEARCH:
					try {
						JSONObject json = new JSONObject(result);
						if (json.getString("SearchResult").equals("TPB")) {
							TPBResponse tpbResponse = Utils.processTPBDetailsJSON(result);
							CorsairFragment cFragment = _parent.getCorsairFragment();
							if (cFragment != null) {
								cFragment.displaySearchResult(tpbResponse);
							}
						}
					} catch (JSONException jse) {
						Log.e("FREEBOOTER", "Error trying to parse TPB search result");
						Log.e("FREEBOOTER", jse.getMessage());
					} finally {
						_parent.enableSettingsMenu();
					}
					break;
				case CMD_TPB_DETAILS:
					TPBObjectDetails t = Utils.processTPBInfoJSON(result, context);
					TPBDetails tDetails = new TPBDetails(t);
					FragmentTransaction ft = _parent.getFragmentManager().beginTransaction();
					_parent.dismissProgressDialog();
					tDetails.show(ft, "Corsair");
					break;
				case CMD_STATUS:
					if (_lastResponseStatus == 200) {
						try {
							JSONObject json = new JSONObject(result);
							if (json.getString("Object").equals("DownloadProgress")) {
								Utils.processAndUpdateDownloadProgressData(_parent, result);
							}
						} catch (JSONException jse) {
							Log.e("FREEBOOTER", "Error trying to parse Progress JSON object!");
							Log.e("FREEBOOTER", jse.getMessage());
						}
					} else {
						Toast.makeText(_parent, _parent.getString(R.string.warning_rest_result) + " " + result, Toast.LENGTH_LONG).show();
						Log.e("FREEBOOTER", "Error processing status: " + result);
						_parent.cancelProgressAlarm();
					}
					break;
			}
		} else {
			Toast.makeText(_parent, R.string.warning_no_response, Toast.LENGTH_LONG).show();
			YIFYFragment yifyFragment = _parent.getYIFYFragment();
			if (yifyFragment != null) {
				yifyFragment.setStatus(_parent.getString(R.string.msg_not_connected));
			}
			Utils.vibrate(_parent);
		}
		return true;
	}
}
