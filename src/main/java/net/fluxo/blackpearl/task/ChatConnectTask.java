/*
 * ChatConnectTask.java
 *
 * Copyright (c) 2014 Ronald Kurniawan. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 */
package net.fluxo.blackpearl.task;

import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.os.Build;
import android.util.Log;
import net.fluxo.blackpearl.MainActivity;
import net.fluxo.blackpearl.R;
import net.fluxo.blackpearl.taskhandler.IgnitedAsyncTask;
import org.jivesoftware.smack.ConnectionConfiguration;
import org.jivesoftware.smack.SASLAuthentication;
import org.jivesoftware.smack.XMPPConnection;
import org.jivesoftware.smack.XMPPException;

import java.io.File;

/**
 * This Task deals with connecting to the XMPP server.
 *
 * @author Ronald Kurniawan (viper)
 * @version 0.2.6, 28/03/2014
 */
public class ChatConnectTask extends IgnitedAsyncTask<MainActivity, Void, Void, XMPPConnection> {

	private MainActivity _parent;
	private SharedPreferences _pref;
	private ProgressDialog _pd;

	public ChatConnectTask(MainActivity parent, SharedPreferences settings) {
		super();
		_parent = parent;
		_pref = settings;
		lockScreenOrientation();
		_parent.dismissChatRetryConnectDialog();
		_pd = new ProgressDialog(_parent);
		_pd.setMessage(_parent.getString(R.string.msg_connecting));
		_pd.setCancelable(false);
		_pd.show();
	}

	/**
	 * Close the ProgressDialog if it's displayed.
	 */
	public void dismissProgressDialog() {
		if (_pd != null && _pd.isShowing()) {
			_pd.dismiss();
			_pd = null;
		}
	}

	/**
	 * Attempts to connect to the XMPP server and authenticate the client's identity.
	 * @param params (not used) the parameters for your task
	 * @return an XMPPConnection object
	 * @throws Exception
	 */
	@Override
	public XMPPConnection run(Void... params) throws Exception {
		String prefNetwork = _pref.getString("freebooter_pref_network", "Facebook").toLowerCase();
		ConnectionConfiguration cc = null;
		if (prefNetwork.equals("facebook")) {
			cc = new ConnectionConfiguration("chat.facebook.com", 5222, "chat.facebook.com");
		} else if (prefNetwork.equals("google")) {
			cc = new ConnectionConfiguration("talk.google.com", 5222, "gmail.com");
		}
		assert cc != null;
		cc.setReconnectionAllowed(true);
		cc.setSendPresence(true);
		cc.setSASLAuthenticationEnabled(true);
		cc.setRosterLoadedAtLogin(true);
		cc.setSecurityMode(ConnectionConfiguration.SecurityMode.enabled);
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.ICE_CREAM_SANDWICH) {
			cc.setTruststoreType("AndroidCAStore");
			cc.setTruststorePassword(null);
			cc.setTruststorePath(null);
		} else {
			cc.setTruststoreType("BKS");
			String path = System.getProperty("javax.net.ssl.trustStore");
			if (path == null)
				path = System.getProperty("java.home") + File.separator + "etc"
					+ File.separator + "security" + File.separator
					+ "cacerts.bks";
			cc.setTruststorePath(path);
		}
		// Connecting now...
		XMPPConnection chatConnection = new XMPPConnection(cc);
		Log.i("FREEBOOTER", "Connecting to chat server...");
		try {
			SASLAuthentication.supportSASLMechanism("PLAIN", 0);
			chatConnection.connect();
			int counter = 0;
			while (!chatConnection.isConnected() && counter < 10) {
				try {
					Thread.sleep(1000);
					counter++;
				} catch (InterruptedException ignored) { }
			}
			if (!chatConnection.isConnected()) {
				return null;
			}
		} catch (XMPPException xmppe) {
			Log.e("FREEBOOTER", "Error connecting to chat server!");
			Log.e("FREEBOOTER", xmppe.getMessage());
			return null;
		}

		int retries = 5;
		while (!chatConnection.isAuthenticated() && retries >= 0) {
			try {
				Log.i("FREEBOOTER", "Authenticating to chat server, retries: " + retries);
				chatConnection.login(_pref.getString("freebooter_pref_chat_username", ""),
					_pref.getString("freebooter_pref_chat_password", ""));
			} catch (XMPPException xmppe) {
				Log.e("FREEBOOTER", "Error authenticating to chat server!");
				Log.e("FREEBOOTER", xmppe.getMessage());
				retries--;
			}
		}
		if (!chatConnection.isAuthenticated()) {
			return null;
		}
		Log.i("FREEBOOTER", "Chat connection authenticated");

		return chatConnection;
	}

	/**
	 * Defines what happens after the Task is completed.
	 *
	 * @param context The most recent instance of the Context that executed this IgnitedAsyncTask the
	 * @param result the XMPPConnection object from run()
	 * @return true
	 */
	@Override
	public boolean onTaskCompleted(MainActivity context, XMPPConnection result) {
		super.onTaskCompleted(context, result);
		if (result != null) {
			_parent.setChatData(result);
		} else {
			_parent.showRetryConnectDialog();
		}
		dismissProgressDialog();
		unlockScreenOrientation();
		return true;
	}

	/**
	 * Locks the screen orientation.
	 */
	private void lockScreenOrientation() {
		int currentOrientation = _parent.getResources().getConfiguration().orientation;
		if (currentOrientation == Configuration.ORIENTATION_PORTRAIT) {
			_parent.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		} else {
			_parent.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
		}
	}

	/**
	 * Unlocks the screen orientation.
	 */
	private void unlockScreenOrientation() {
		_parent.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR);
	}

}
