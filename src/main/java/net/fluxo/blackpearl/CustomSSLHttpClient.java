/*
 * CustomSSLHttpClient.java
 *
 * Copyright (c) 2014 Ronald Kurniawan. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 */
package net.fluxo.blackpearl;

import android.content.Context;
import android.util.Log;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.conn.ssl.X509HostnameVerifier;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.SingleClientConnManager;

import javax.net.ssl.SSLException;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocket;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.security.KeyStore;
import java.security.cert.X509Certificate;

/**
 * A HTTP client based on Apache's Default Http client. This class is specifically
 * developed for communication with the daemon via TLS with the self-signed certificate.
 * User must insert their own keystore password and daemon's IP address in the code before
 * compiling the project.
 *
 * @author Ronald Kurniawan (viper)
 * @version 0.2.6, 21/05/14
 */
public class CustomSSLHttpClient extends DefaultHttpClient {

	private final Context _context;

	/**
	 * URL to send request to.
	 */
	private final String _url;

	/**
	 * Password to client's own keystore. User must change the value of this
	 * property with their own value before compilation.
	 */
	protected final String _keystorePassword = "[KEYSTORE-PASSWORD]";

	public CustomSSLHttpClient(Context ctx, String url) {
		_context = ctx;
		_url = url;
	}

	@Override
	protected ClientConnectionManager createClientConnectionManager() {
		SchemeRegistry registry = new SchemeRegistry();
		int securePort = 443;
		try {
			URL url = new URL(_url);
			if (url.getPort() > 0) {
				securePort = url.getPort();
			}
		} catch (Exception ignored) { }
		registry.register(new Scheme("http", PlainSocketFactory.getSocketFactory(), 8889));
		registry.register(new Scheme("https", newSSLSocketFactory(), securePort));
		return new SingleClientConnManager(getParams(), registry);
	}

	/**
	 * Creates an instance of new SSL Socket Factory with the details of client's own keystore
	 * for use with this connection.
	 * @return an SSLSocketFactory object
	 */
	private SSLSocketFactory newSSLSocketFactory() {
		try {
			KeyStore keyStore = KeyStore.getInstance("BKS");
			InputStream in = _context.getResources().openRawResource(R.raw.jettytest);
			try {
				keyStore.load(in, _keystorePassword.toCharArray());
			} finally {
				in.close();
			}
			SSLSocketFactory sf = new SSLSocketFactory(keyStore);
			// Don't know if this one is really needed!
			sf.setHostnameVerifier(new CustomHostnameVerifier());
			return sf;
		} catch (Exception e) {
			Log.e("FREEBOOTER", "SSL exception: " + e.getMessage());
			throw new AssertionError(e);
		}
	}

	/**
	 * Custom class of hostname verifier to verify client's self-signed certificate.
	 */
	class CustomHostnameVerifier implements X509HostnameVerifier {

		/**
		 * Domain name value on the self-signed certificate must match this property.
		 * User must change the value of this property before compilation.
		 */
		private final String MY_CERT_DOMAIN = "Test Jetty";

		/**
		 * IP address of the server. Only use this if you have a static IP address that
		 * can be reached externally. User must change the value of this property before compilation.
		 */
		private final String MY_STATIC_ADDRESS = "[YOUR-OWN-SERVER'S-IP-ADDRESS]";

		/**
		 * Verifies the value of self-signed certificate's Domain property with the value
		 * specified in this verifier class.
		 *
		 * @param host hostname
		 * @param session SSL session
		 * @return true if values match; false otherwise
		 */
		@Override
		public boolean verify(String host, SSLSession session) {
			String sslHost = session.getPeerHost();
			return MY_CERT_DOMAIN.toLowerCase().equals(sslHost);
		}

		/**
		 * Verifies the value of daemon's IP address with the value specified in this verifier class.
		 * @param host hostname
		 * @param ssl SSL socket
		 * @throws IOException thrown when values don't match
		 */
		@Override
		public void verify(String host, SSLSocket ssl) throws IOException {
			String sslHost = ssl.getInetAddress().getHostName();
			if (!MY_STATIC_ADDRESS.equals(sslHost)) {
				throw new IOException("hostname in certificate didn't match: " + host + " != " + sslHost);
			}
		}

		@Override
		public void verify(String host, X509Certificate cert) throws SSLException {
			throw new SSLException("Hostname verification 1 not implemented");
		}

		@Override
		public void verify(String host, String[] cns, String[] subjectAlts) throws SSLException {
			throw new SSLException("Hostname verification 2 not implemented");
		}
	}
}
