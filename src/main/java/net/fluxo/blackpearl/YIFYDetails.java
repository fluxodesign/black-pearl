/*
 * YIFYDetails.java
 *
 * Copyright (c) 2014 Ronald Kurniawan. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 */
package net.fluxo.blackpearl;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.koushikdutta.urlimageviewhelper.UrlImageViewHelper;
import net.fluxo.blackpearl.dto.YIFYObjectDetails;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * This class displays a DialogFragment containing details of an item from
 * YIFY server.
 *
 * @author Ronald Kurniawan (viper)
 * @version 0.2.6, 2/04/14
 * @see android.app.DialogFragment
 */
public class YIFYDetails extends DialogFragment {

	/**
	 * Details of an object from YIFY server.
	 */
	private YIFYObjectDetails _details;

	public YIFYDetails(YIFYObjectDetails yod) {
		_details = yod;
	}

	/**
	 * Called when this DialogFragment is created. This DialogFragment should have no style
	 * and uses Theme Black with no title bar.
	 * @param savedInstanceState a Bundle object
	 */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setStyle(DialogFragment.STYLE_NO_TITLE, android.R.style.Theme_Black_NoTitleBar);
	}

	/**
	 * Inflates the layout for YIFY details and populate the values in the layout using
	 * the _details property.
	 *
	 * @param savedInstanceState a Bundle object
	 * @return a Dialog object
	 */
	@SuppressWarnings("unchecked")
	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		super.onCreateDialog(savedInstanceState);
		if (getActivity() != null) {
			Drawable defaultDrawable = getActivity().getResources().getDrawable(R.drawable.dummy_cover);
			LayoutInflater inflater = getActivity().getLayoutInflater();
			@SuppressLint("InflateParams") View view = inflater.inflate(R.layout.yify_details, null);
			if (view != null) {
				ImageView ivCover = (ImageView) view.findViewById(R.id.iv_details_cover);
				if (_details.getCoverURL().length() > 0 && _details.getCoverURL().startsWith("http")) {
					UrlImageViewHelper.setUrlDrawable(ivCover, _details.getCoverURL(), defaultDrawable);
				} else {
					ivCover.setImageDrawable(defaultDrawable);
				}
				Typeface tf = Utils.getDefaultFont(getActivity());
				TextView tvTitle = (TextView)view.findViewById(R.id.tv_title);
				tvTitle.setTypeface(tf);
				tvTitle.setText(_details.getMovieTitle().toUpperCase() + " (" + _details.getMovieYear() + ")");
				StringBuilder sbDetails = new StringBuilder();
				sbDetails.append("GENRE: ").append(_details.getMovieGenre()).append("\n")
					.append("SIZE: ").append(_details.getMovieSize()).append("\n")
					.append("QUALITY: ").append(_details.getMovieQuality()).append("\n")
					.append("RESOLUTION: ").append(_details.getMovieResolution()).append("\n")
					.append("FRAME RATE: ").append(_details.getFrameRate()).append("\n")
					.append("RUNTIME: ").append(_details.getMovieRuntime()).append(" minutes").append("\n")
					.append("IMDB RATING: ").append(_details.getMovieRating()).append("/10").append("\n")
					.append("PEERS/SEEDS: ").append(_details.getTorrentPeers()).append("/").append(_details.getTorrentSeeds()).append("\n")
					.append("DOWNLOADED: ").append(_details.getDownloadedTimes());
				TextView tvDetails = (TextView)view.findViewById(R.id.tv_details);
				tvDetails.setTypeface(tf);
				tvDetails.setText(sbDetails.toString());
				TextView tvSynopsis = (TextView)view.findViewById(R.id.tv_synopsis);
				tvSynopsis.setTypeface(tf);
				tvSynopsis.setText(_details.getMovieSynopsis());
				TextView tvDirectors = (TextView)view.findViewById(R.id.tv_directors);
				tvDirectors.setTypeface(tf);
				sbDetails.setLength(0);
				Iterator iterator = _details.getDirectors().entrySet().iterator();
				while (iterator.hasNext()) {
					Map.Entry mapEntry = (Map.Entry)iterator.next();
					sbDetails.append("<a href=\"").append(mapEntry.getValue().toString()).append("\">")
						.append(mapEntry.getKey().toString()).append("</a>").append("<br>");
				}
				tvDirectors.setText(Html.fromHtml(sbDetails.toString()));
				TextView tvCast = (TextView)view.findViewById(R.id.tv_cast);
				tvCast.setTypeface(tf);
				sbDetails.setLength(0);
				iterator = _details.getCast().entrySet().iterator();
				// It's <ActorName, <CharacterName, IMDBURL>>
				while (iterator.hasNext()) {
					Map.Entry mapEntry = (Map.Entry)iterator.next();
					Map<String,String> character = (HashMap)mapEntry.getValue();
					Iterator characterIterator = character.entrySet().iterator();
					if (characterIterator.hasNext()) {
						Map.Entry cMapEntry = (Map.Entry) characterIterator.next();
						sbDetails.append("<a href=\"").append(cMapEntry.getValue()).append("\">")
							.append(mapEntry.getKey()).append(" - ").append(cMapEntry.getKey())
							.append("</a>").append("<br>");
					}
				}
				tvCast.setText(Html.fromHtml(sbDetails.toString()));
				tvCast.setTypeface(tf);
				Drawable defaultScreenshot = getActivity().getResources().getDrawable(R.drawable.dummy_screenshot);
				if (_details.getScreenshot1().length() > 0 && _details.getScreenshot1().startsWith("http")) {
					ImageView ivScreenshot1 = (ImageView) view.findViewById(R.id.iv_image01);
					UrlImageViewHelper.setUrlDrawable(ivScreenshot1, _details.getScreenshot1(), defaultScreenshot);
				}
				if (_details.getScreenshot2().length() > 0 && _details.getScreenshot2().startsWith("http")) {
					ImageView ivScreenshot2 = (ImageView) view.findViewById(R.id.iv_image02);
					UrlImageViewHelper.setUrlDrawable(ivScreenshot2, _details.getScreenshot2(), defaultScreenshot);
				}
				if (_details.getScreenshot3().length() > 0 && _details.getScreenshot3().startsWith("http")) {
					ImageView ivScreenshot3 = (ImageView) view.findViewById(R.id.iv_image03);
					UrlImageViewHelper.setUrlDrawable(ivScreenshot3, _details.getScreenshot3(), defaultScreenshot);
				}

				// click listener for POSITIVE listener is REMOVED
				// click listener for Cancel
				DialogInterface.OnClickListener negativeListener = new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialogInterface, int i) {
						dialogInterface.cancel();
					}
				};

				return new AlertDialog.Builder(getActivity())
					.setView(view)
					.setNegativeButton(R.string.layout_cancel, negativeListener).create();
			}
		}

		return null;
	}
}
