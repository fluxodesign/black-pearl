/*
 * ScrollListener.java
 *
 * Copyright (c) 2014 Ronald Kurniawan. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 */
package net.fluxo.blackpearl;

import android.widget.AbsListView;

/**
 * Listener class that defines the ongoing loading of subsequent items when user keeps scrolling
 * down on the View.
 *
 * @author Ronald Kurniawan (viper)
 * @version 0.2.6, 2/04/14
 */
public abstract class ScrollListener implements AbsListView.OnScrollListener {
	/**
	 * The minimum amount of items to have below your current scroll position
	 * before loading more.
 	 */
	private int visibleThreshold = 4;

	/**
	 * The current offset index of data you have loaded.
	 */
	private int currentPage = 0;

	/**
	 * The total number of items in the dataset after the last load.
	 */
	private int previousTotalItemCount = 0;

	/**
	 * True if we are still waiting for the last set of data to load.
	 */
	private boolean loading = true;

	/**
	 * Sets the starting page index.
	 */
	private int startingPageIndex = 0;

	public ScrollListener() {
	}

	public ScrollListener(int visibleThreshold) {
		this.visibleThreshold = visibleThreshold;
	}

	public ScrollListener(int visibleThreshold, int startPage) {
		this.visibleThreshold = visibleThreshold;
		this.startingPageIndex = startPage;
		this.currentPage = startPage;
	}

	/**
	 * This happens many times a second during a scroll, so be wary of the code you place here.
	 * We are given a few useful parameters to help us work out if we need to load some more data,
	 * but first we check if we are waiting for the previous load to finish.
	 * @param view an AbsListView object
	 * @param firstVisibleItem index of first visible item
	 * @param visibleItemCount number of visible items
	 * @param totalItemCount total number of items
	 */
	@Override
	public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount,
	    int totalItemCount) {
		// If the total item count is zero and the previous isn't, assume the
		// list is invalidated and should be reset back to initial state
		if (totalItemCount < previousTotalItemCount) {
			this.currentPage = this.startingPageIndex;
			this.previousTotalItemCount = totalItemCount;
			if (totalItemCount == 0) { this.loading = true; }
		}

		// If it’s still loading, we check to see if the dataset count has
		// changed, if so we conclude it has finished loading and update the current page
		// number and total item count.
		if (loading && (totalItemCount > previousTotalItemCount)) {
			loading = false;
			previousTotalItemCount = totalItemCount;
			currentPage++;
		}

		// If it isn’t currently loading, we check to see if we have breached
		// the visibleThreshold and need to reload more data.
		// If we do need to reload some more data, we execute onLoadMore to fetch the data.
		if (!loading && (totalItemCount - visibleItemCount)<=(firstVisibleItem + visibleThreshold)) {
			onLoadMore(currentPage + 1, totalItemCount);
			loading = true;
		}
	}

	/**
	 * Defines the process for actually loading more data based on page.
	 * @param page page number
	 * @param totalItemsCount number of items
	 */
	public abstract void onLoadMore(int page, int totalItemsCount);

	@Override
	public void onScrollStateChanged(AbsListView view, int scrollState) {
		// Don't take any action on changed
	}
}
