/*
 * SpinnerItemSelectedListener.java
 *
 * Copyright (c) 2014 Ronald Kurniawan. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 */
package net.fluxo.blackpearl;

import android.view.View;
import android.widget.AdapterView;
import net.fluxo.blackpearl.interfaces.ISpinnerSelectedListener;

/**
 * This is the Listener for the Spinner used in the YIFY's search settings.
 *
 * @author Ronald Kurniawan (viper)
 * @version 0.2.6, 9/04/14
 */
public class SpinnerItemSelectedListener implements AdapterView.OnItemSelectedListener {

	private int _type = -1;
	public static final int T_RATING = 1;
	public static final int T_QUALITY = 2;
	private ISpinnerSelectedListener _spinnerListener;

	public SpinnerItemSelectedListener(int type, ISpinnerSelectedListener listener) {
		_spinnerListener = listener;
		switch (type) {
			case T_RATING: _type = T_RATING; break;
			case T_QUALITY: _type = T_QUALITY; break;
		}
	}

	@Override
	public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
		_spinnerListener.setFilterValue(_type, adapterView.getItemAtPosition(i).toString());
	}

	@Override
	public void onNothingSelected(AdapterView<?> adapterView) {

	}

}
