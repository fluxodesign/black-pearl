/*
 * SettingsActivity.java
 *
 * Copyright (c) 2014 Ronald Kurniawan. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 */
package net.fluxo.blackpearl;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;

/**
 * Activity for displaying user preferences.
 *
 * @author Ronald Kurniawan (viper)
 * @version 0.2.6, 28/03/2014
 */
public class SettingsActivity extends Activity {

	/**
	 * Called when this Activity is created.
	 * @param savedStateInstance a Bundle object
	 */
	@Override
	public void onCreate(Bundle savedStateInstance) {
		super.onCreate(savedStateInstance);

		// Display the fragment as main content...
		getFragmentManager().beginTransaction().replace(android.R.id.content, new SettingsFragment())
			.commit();
	}

	/**
	 * Called when this Activity is paused.
	 */
	@Override
	public void onPause() {
		super.onPause();
		SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(this);
		String newCommMethod = pref.getString("freebooter_pref_comm", "");
		if (newCommMethod.length() > 0) {
			Intent intent = new Intent("net.fluxo.mediaminer.COMM_METHOD_CHANGED")
				.putExtra("new_method", newCommMethod);
			sendBroadcast(intent);
		}
	}
}
