/*
 * TPBFilterDialog.java
 *
 * Copyright (c) 2014 Ronald Kurniawan. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 */
package net.fluxo.blackpearl;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.CheckBox;
import android.widget.ExpandableListView;

/**
 * A class that builds the AlertDialog to show the categories and subcategories available on TPB site. It is needed to
 * filter the search.
 *
 * @author Ronald Kurniawan (viper)
 * @version 0.2.6, 22/04/14
 */
public class TPBFilterDialog {

	private Context _ctx;

	public TPBFilterDialog(Context context) {
		_ctx = context;
	}

	/**
	 * Inflates the layout for the Filter Dialog and populates the values inside the layout (categories and subcategories).
	 * @return AlertDialog to show the TPB categories and subcategories
	 */
	public AlertDialog build() {
		LayoutInflater inflater = (LayoutInflater)_ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		@SuppressLint("InflateParams") View v = inflater.inflate(R.layout.tpb_filter, null);
		if (v != null) {
			CheckBox cbCategoryAll = (CheckBox)v.findViewById(R.id.cb_tpb_filter_all);
			final ExpandableListView elvCategories = (ExpandableListView)v.findViewById(R.id.elv_tpb_filter_cat);
			final TPBCategoryAdapter elvAdapter = new TPBCategoryAdapter(_ctx);
			elvCategories.setAdapter(elvAdapter);
			cbCategoryAll.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View view) {
					boolean checked = ((CheckBox)view).isChecked();
					elvCategories.setVisibility(checked ? View.INVISIBLE : View.VISIBLE);
					if (elvCategories.getExpandableListAdapter() != null) {
						if (checked) {
							((TPBCategoryAdapter) elvCategories.getExpandableListAdapter()).getSelectedSubcategories().clear();
							((TPBCategoryAdapter) elvCategories.getExpandableListAdapter()).getSelectedSubcategories().add("0");
						} else {
							((TPBCategoryAdapter) elvCategories.getExpandableListAdapter()).getSelectedSubcategories().clear();
						}
					}
					if (checked) {
						CorsairFragment corsairFragment = ((MainActivity)_ctx).getCorsairFragment();
						if (corsairFragment != null) {
							corsairFragment.setSearchCategory("0");
						}
					}
				}
			});

			// display the filter as it was shown before...
			CorsairFragment cFragment = ((MainActivity)_ctx).getCorsairFragment();
			if (cFragment != null) {
				if (cFragment.getSearchCategory().equals("0")) {
					cbCategoryAll.setChecked(true);
				} else {
					cbCategoryAll.setChecked(false);
					elvCategories.setVisibility(View.VISIBLE);
					if (elvCategories.getExpandableListAdapter() != null) {
						((TPBCategoryAdapter) elvCategories.getExpandableListAdapter()).deprocessSubcats(cFragment.getSearchCategory());
					}
				}
			}

			return new AlertDialog.Builder(_ctx)
				.setTitle(R.string.title_tpb_categories)
				.setCancelable(false)
				.setView(v)
				.setPositiveButton(R.string.btn_ok, new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialogInterface, int i) {

					}
				})
				.setNegativeButton(R.string.btn_cancel, new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialogInterface, int i) {
						dialogInterface.dismiss();
					}
				}).create();
		}
		return null;
	}
}
