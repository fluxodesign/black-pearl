/*
 * TPBObjectDetails.java
 *
 * Copyright (c) 2014 Ronald Kurniawan. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 */
package net.fluxo.blackpearl.dto;

/**
 * This Data Transfer Object represents details of an item returned from TPB search result.
 *
 * @author Ronald Kurniawan (viper)
 * @version 0.2.6, 14/04/14
 */
public class TPBObjectDetails {

	/**
	 * Number of seeders for this item.
	 */
	private int _seeders = 0;

	/**
	 * Setter method for _seeders property.
	 * @param value number of seeders for this item
	 */
	public void setSeeders(int value) { _seeders = value; }

	/**
	 * Getter mthod for _seeders property.
	 * @return number of seeders for this item
	 */
	public int getSeeders() { return _seeders; }

	/**
	 * Number of leechers for this item.
	 */
	private int _leechers = 0;

	/**
	 * Setter method for _leechers property.
	 * @param value number of leechers for this item
	 */
	public void setLeechers(int value) { _leechers = value; }

	/**
	 * Getter method for _leechers property.
	 * @return number of leechers for this item
	 */
	public int getLeechers() { return _leechers; }

	/**
	 * Category number (TPB)
	 */
	private int _type = 0;

	/**
	 * Setter method for _type property.
	 * @param value category number (defined by TPB)
	 */
	public void setType(int value) { _type = value; }

	/**
	 * Getter method for _type property.
	 * @return category number (defined by TPB)
	 */
	public int getType() { return _type; }

	/**
	 * Title of this item
	 */
	private String _title = "";

	/**
	 * Setter method for _title property.
	 * @param value title of this item
	 */
	public void setTitle(String value) { _title = value; }

	/**
	 * Getter method for _title property.
	 * @return title of this item
	 */
	public String getTitle() { return _title; }

	/**
	 * Date and time when this item was uploaded.
	 */
	private String _uploaded = "";

	/**
	 * Setter method for _uploaded property.
	 * @param value date and time when item was uploaded
	 */
	public void setUploaded(String value) { _uploaded = value; }

	/**
	 * Getter method for _uploaded property.
	 * @return date and time when item was uploaded
	 */
	public String getUploaded() { return _uploaded; }

	/**
	 * Size of the item.
	 */
	private String _size = "";

	/**
	 * Setter method for _size property.
	 * @param value size of the item
	 */
	public void setSize(String value) { _size = value; }

	/**
	 * Getter method for _size property.
	 * @return size of the item
	 */
	public String getSize() { return _size; }

	/**
	 * Uploader's name.
	 */
	private String _uploader = "";

	/**
	 * Setter method for _uploader property.
	 * @param value uploader's name
	 */
	public void setUploader(String value) { _uploader = value; }

	/**
	 * Getter method for _uploader property.
	 * @return uploader's name
	 */
	public String getUploader() { return _uploader; }

	/**
	 * URL for item's detail.
	 */
	private String _detailsURL = "";

	/**
	 * Setter method for _detailsURL property.
	 * @param value URL for item's detail
	 */
	public void setDetailsURL(String value) { _detailsURL = value; }

	/**
	 * Getter method for _detailsURL property.
	 * @return URL for item's detail
	 */
	public String getDetailsURL() { return _detailsURL; }

	/**
	 * Bittorent Magnet URL for the item.
	 */
	private String _magnetURL = "";

	/**
	 * Setter method for _magnetURL property.
	 * @param value bittorent magnet URL
	 */
	public void setMagnetURL(String value) { _magnetURL = value; }

	/**
	 * Getter method for _magnetURL property.
	 * @return bittorent magnet URL
	 */
	public String getMagnetURL() { return _magnetURL; }

	/**
	 * URL to Bittorent file.
	 */
	private String _torrentURL = "";

	/**
	 * Setter method for _torrentURL property.
	 * @param value URL to torrent file
	 */
	public void setTorrentURL(String value) { _torrentURL = value; }

	/**
	 * Getter method for _torrentURL property.
	 * @return URL to torrent file
	 */
	public String getTorrentURL() { return _torrentURL; }

	/**
	 * Info for this item.
	 */
	private String _info = "";

	/**
	 * Setter method for _info property.
	 * @param value info for this item
	 */
	public void setInfo(String value) { _info = value; }

	/**
	 * Getter method for _info property.
	 * @return info for this item
	 */
	public String getInfo() { return _info; }
}
