/*
 * ServerChatID.java
 *
 * Copyright (c) 2014 Ronald Kurniawan. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 */
package net.fluxo.blackpearl.dto;

/**
 * Data Transfer Object representing daemon's identity for the communication with XMPP server.
 *
 * @author Ronald Kurniawan (viper)
 * @version 0.2.6, 1/04/14
 */
public class ServerChatID {

	/**
	 * User ID of the daemon.
	 */
	private String _id;

	/**
	 * Name of the daemon.
	 */
	private String _name;

	/**
	 * Is daemon available for chat?
	 */
	private boolean _available = false;

	/**
	 * Constructor for the class.
	 * @param name name of the daemon
	 */
	public ServerChatID(String name) {
		_name = name;
	}

	/**
	 * Getter method for _name property.
	 * @return name of the daemon
	 */
	public String getName() { return  _name; }

	/**
	 * Setter method for _id property.
	 * @param value user ID for the daemon
	 */
	public void setID(String value) { _id = value; }

	/**
	 * Getter method for _id property.
	 * @return user ID for the daemon
	 */
	public String getID() { return _id; }

	/**
	 * Getter method for _available property.
	 * @return true if daemon is available for chat; false otherwise
	 */
	public boolean isAvailable() { return _available; }

	/**
	 * Setter method for _available property.
	 * @param value true if daemon is available; false otherwise
	 */
	public void setAvailable(boolean value) { _available = value; }

}
