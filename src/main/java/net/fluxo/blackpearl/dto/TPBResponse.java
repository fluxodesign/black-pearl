/*
 * TPBResponse.java
 *
 * Copyright (c) 2014 Ronald Kurniawan. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 */
package net.fluxo.blackpearl.dto;

import java.util.ArrayList;
import java.util.List;

/**
 * A Data Transfer Object representing a single search result from TPB server.
 *
 * @author Ronald Kurniawan (viper)
 * @version 0.2.6, 17/04/14
 */
public class TPBResponse {

	/**
	 * Total items returned from the search.
	 */
	private int _itemCount = 0;

	/**
	 * Getter method for _itemCount property.
	 * @return total items returned from the search
	 */
	public int getItemCount() { return _itemCount; }

	/**
	 * Setter method for _itemCount property.
	 * @param value total items returned from the search
	 */
	public void setItemCount(int value) { _itemCount = value; }

	/**
	 * List object containing the item details from the search results.
	 */
	private List<TPBObjectDetails> _list;

	/**
	 * Getter method for _list property.
	 * @return a List containing item details from the search results
	 */
	public List<TPBObjectDetails> getItems() {
		if (_list == null) {
			_list = new ArrayList<>();
		}
		return _list;
	}

	/**
	 * Add a single item details to the List.
	 * @param obj a TPBObjectDetails
	 */
	public void addItemsToList(TPBObjectDetails obj) {
		if (_list == null) {
			_list = new ArrayList<>();
		}
		_list.add(obj);
	}
}
