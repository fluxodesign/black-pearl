/*
 * DownloadProgress.java
 *
 * Copyright (c) 2014 Ronald Kurniawan. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 */
package net.fluxo.blackpearl.dto;

/**
 * Data Transfer Object representing the download progress report returned by our daemon.
 *
 * @author Ronald Kurniawan (viper)
 * @version 0.2.6, 4/06/14
 */
public class DownloadProgress {

	/**
	 * Constructor for the class.
	 * @param title Title of the downloaded item
	 * @param progress a value from 0 to 100
	 */
	public DownloadProgress(String title, int progress) {
		if (title != null && title.length() > 0) {
			setTitle(title);
		}
		if (progress > -1) {
			setProgress(progress);
		}
	}

	/**
	 * Title of the downloaded item.
	 */
	private String _title = "";

	/**
	 * Setter method for _title property.
	 * @param value title of the downloaded item
	 */
	public void setTitle(String value) {
		_title = value;
	}

	/**
	 * Getter method for _title property.
	 * @return title of the downloaded item
	 */
	public String getTitle() {
		return _title;
	}

	/**
	 * Progress of the download.
	 */
	private int _progress = -1;

	/**
	 * Setter method for _progress property.
	 * @param value progress of the download (0 - 100)
	 */
	public void setProgress(int value) {
		_progress = value;
	}

	/**
	 * Getter method for _progress property.
	 * @return progress of the download (0 - 100)
	 */
	public int getProgress() {
		return _progress;
	}
}
