/*
 * YIFYObject.java
 *
 * Copyright (c) 2014 Ronald Kurniawan. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 */
package net.fluxo.blackpearl.dto;

/**
 * This is a Data Transfer Object representing details of the item returned in a response
 * from YIFY server.
 *
 * @author Ronald Kurniawan (viper)
 * @version 0.2.6, 28/03/2014
 */
public class YIFYObject {

	/**
	 * Movie ID property from YIFY server
	 */
	private String _movieID = "";

	/**
	 * Getter method for _movieID property.
	 * @return movie ID
	 */
	public String getMovieID() { return _movieID; }

	/**
	 * Setter method for _movieID property.
	 * @param value movie ID
	 */
	public void setMovieID(String value) { _movieID = value; }

	/**
	 * Movie State property as described by the enumeration MovieState.
	 */
	private MovieState _state = MovieState.OK;

	/**
	 * Getter method for _state property.
	 * @return the MovieState for this item
	 */
	public MovieState getMovieState() { return _state; }

	/**
	 * Setter method for _state property.
	 * @param value the MovieState for this item
	 */
	public void setMovieState(MovieState value) { _state = value; }

	/**
	 * URL for this item on the YIFY server.
	 */
	private String _movieUrl = "";

	/**
	 * Getter method for _movieUrl property.
	 * @return the URL for this item
	 */
	public String getMovieURL() { return _movieUrl; }

	/**
	 * Setter method for _movieUrl property.
	 * @param value the URL for this item
	 */
	public void setMovieURL(String value) { _movieUrl = value; }

	/**
	 * Title for this item.
	 */
	private String _title = "";                 // Choose the MovieTitleClean

	/**
	 * Getter method for _title property.
	 * @return title for this item
	 */
	public String getMovieTitle() { return _title; }

	/**
	 * Setter method for _title property.
	 * @param value title for this item
	 */
	public void setMovieTitle(String value) { _title = value; }

	/**
	 * Movie release year.
	 */
	private String _movieYear = "";

	/**
	 * Getter method for _movieYear property.
	 * @return release year of the movie
	 */
	public String getMovieYear() { return _movieYear; }

	/**
	 * Setter method for _movieYear property.
	 * @param value release year of the movie
	 */
	public void setMovieYear(String value) { _movieYear = value; }

	/**
	 * Date when the item was uploaded.
	 */
	private String _dateUploaded = "";

	/**
	 * Getter method of _dateUploaded property.
	 * @return a String containing the date when the item was uploaded
	 */
	public String getDateUploaded() { return _dateUploaded; }

	/**
	 * Setter method of _dateUploaded property.
	 * @param value a String containing the date when the item was uploaded
	 */
	public void setDateUploaded(String value) { _dateUploaded = value; }

	/**
	 * Milliseconds since epoch of the time when item was uploaded.
	 */
	private long _dateUploadedMillis = 0;       // NZ

	/**
	 * Getter method for _dateUploadedMillis property.
	 * @return a Long value containing the millis when item was uploaded
	 */
	public long getDateUploadedMillis() { return _dateUploadedMillis; }

	/**
	 * Setter method for _dateUploadedMillis property.
	 * @param value a Long value containing the millis when item was uploaded
	 */
	public void setDateUploadedMillis(long value) { _dateUploadedMillis = value; }

	/**
	 * Quality of the item. At the moment there are 3 possible values: 720p, 1080p and 3D.
	 */
	private String _quality = "";

	/**
	 * Getter method for _quality method.
	 * @return a String containing the quality of the item
	 */
	public String getMovieQuality() { return _quality; }

	/**
	 * Setter method for _quality method.
	 * @param value a String containing the quality of the item
	 */
	public void setMovieQuality(String value) { _quality = value; }

	/**
	 * URL for the cover image of the item.
	 */
	private String _coverURL = "";

	/**
	 * Getter method for _coverURL property.
	 * @return URL for cover image
	 */
	public String getCoverURL() { return _coverURL; }

	/**
	 * Setter method for _coverURL property.
	 * @param value URL for cover image
	 */
	public void setCoverURL(String value) { _coverURL = value; }

	/**
	 * IMDB code for the item.
	 */
	private String _imdbCode = "";

	/**
	 * Getter method for _imdbCode property.
	 * @return IMDB code
	 */
	public String getIMDBCode() { return _imdbCode; }

	/**
	 * Setter method for _imdbCode property.
	 * @param value IMDB code
	 */
	public void setIMDBCode(String value) { _imdbCode = value; }

	/**
	 * URL to IMDB page for this item.
	 */
	private String _imdbURL = "";

	/**
	 * Getter method for _imdbURL property.
	 * @return URL for IMDB page for this item
	 */
	public String getIMDBURL() { return _imdbURL; }

	/**
	 * Setter method for _imdbURL property.
	 * @param value URL for IMDB page for this item
	 */
	public void setIMDBURL(String value) { _imdbURL = value; }

	/**
	 * Size of the item (represented in GB or MB).
	 */
	private String _size = "";

	/**
	 * Getter method of _size property.
	 * @return a String containing the size of the item (GB or MB)
	 */
	public String getMovieSize() { return _size; }

	/**
	 * Setter method of _size property.
	 * @param value a String containing the size of the item (GB or MB)
	 */
	public void setMovieSize(String value) { _size = value; }

	/**
	 * Size of the item (in bytes).
	 */
	private long _sizeBytes = 0;

	/**
	 * Getter method for _sizeBytes property.
	 * @return the size of item in bytes
	 */
	public long getMovieSizeInBytes() { return _sizeBytes; }

	/**
	 * Setter method for _sizeBytes property.
	 * @param value the size of item in bytes
	 */
	public void setMovieSizeInBytes(long value) { _sizeBytes = value; }

	/**
	 * Item's IMDB rating, 0.0 to 10.0
	 */
	private String _movieRating = "";

	/**
	 * Getter method for _movieRating property.
	 * @return IMDB rating
	 */
	public String getMovieRating() { return _movieRating; }

	/**
	 * Setter method for _movieRating property.
	 * @param value IMDB rating
	 */
	public void setMovieRating(String value) { _movieRating = value; }

	/**
	 * Genre of the movie.
	 */
	private String _movieGenre = "";

	/**
	 * Getter method of _movieGenre property.
	 * @return genre of the movie
	 */
	public String getMovieGenre() { return _movieGenre; }

	/**
	 * Setter method of _movieGenre property.
	 * @param value genre of the movie
	 */
	public void setMovieGenre(String value) { _movieGenre = value; }

	/**
	 * Name of the uploader of the item.
	 */
	private String _uploader = "";

	/**
	 * Getter method of _uploader property.
	 * @return uploader's name
	 */
	public String getMovieUploader() { return _uploader; }

	/**
	 * Setter method of _uploader property.
	 * @param value uploader's name
	 */
	public void setMovieUploader(String value) { _uploader = value; }

	/**
	 * How many times this item has been downloaded.
	 */
	private int _downloaded = 0;

	/**
	 * Getter method for _downloaded property.
	 * @return number of times item has been downloaded
	 */
	public int getDownloadedTimes() { return _downloaded; }

	/**
	 * Setter method for _downloaded property.
	 * @param value number of times item has been downloaded
	 */
	public void setDownloadedTimes(int value) { _downloaded = value; }

	/**
	 * Number of seeds this item has.
	 */
	private int _torrentSeeds = 0;

	/**
	 * Getter method for _torrentSeeds property.
	 * @return number of seeds this item has
	 */
	public int getTorrentSeeds() { return _torrentSeeds; }

	/**
	 * Setter method for _torrentSeeds property.
	 * @param value number of seeds this item has
	 */
	public void setTorrentSeeds(int value) { _torrentSeeds = value; }

	/**
	 * Number of peers this item has.
	 */
	private int _torrentPeers = 0;

	/**
	 * Getter method for _torrentPeers property.
	 * @return number of peers this item has
	 */
	public int getTorrentPeers() { return _torrentPeers; }

	/**
	 * Setter method for _torrentPeers property.
	 * @param value number of peers this item has
	 */
	public void setTorrentPeers(int value) { _torrentPeers = value; }

	/**
	 * URL for bittorrent file for this item.
	 */
	private String _torrentURL = "";

	/**
	 * Getter method for _torrentURL property.
	 * @return URL for bittorrent file
	 */
	public String getTorrentURL() { return _torrentURL; }

	/**
	 * Setter method for _torrentURL property.
	 * @param value URL for bittorrent file
	 */
	public void setTorrentURL(String value) { _torrentURL = value; }

	/**
	 * Hash value for item's torrent.
	 */
	private String _torrentHash = "";

	/**
	 * Getter method for _torrentHash property.
	 * @return hash value for torrent
	 */
	public String getTorrentHash() { return _torrentHash; }

	/**
	 * Setter method for _torrentHash property.
	 * @param value hash value for torrent
	 */
	public void setTorrentHash(String value) { _torrentHash = value; }

	/**
	 * Bittorent's magnet URL.
	 */
	private String _torrentMagnetURL = "";

	/**
	 * Getter method for _torrentMagnetURL.
	 * @return Bittorrent Magnet URL
	 */
	public String getTorrentMagnetURL() { return _torrentMagnetURL; }

	/**
	 * Setter method for _torrentMagnetURL.
	 * @param value Bittorent Magnet URL
	 */
	public void setTorrentMagnetURL(String value) { _torrentMagnetURL = value; }

	/**
	 * Enumeration for Movie State.
	 */
	public enum MovieState {
		OK(0),
		DISABLED(1),
		REMOVED_DMCA(2);

		private final int _value;

		MovieState(int value) {
			_value = value;
		}

		int getValue() { return _value; }
	}
}
