/*
 * YIFYObjectDetails.java
 *
 * Copyright (c) 2014 Ronald Kurniawan. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 */
package net.fluxo.blackpearl.dto;

import java.util.HashMap;
import java.util.Map;

/**
 * This is a Data Transfer Object representing the details of a single item returned from
 * the YIFY server, enclosed in a response.
 *
 * @author Ronald Kurniawan (viper)
 * @version 0.2.6, 2/04/14
 */
public class YIFYObjectDetails extends YIFYObject {

	/**
	 * Resolution for this item. At the moment, there are 3 possible values:
	 * 720p, 1080p and 3D.
	 */
	private String _resolution = "";

	/**
	 * Getter method for _resolution property.
	 * @return a String representing this item's resolution
	 */
	public String getMovieResolution() { return _resolution; }

	/**
	 * Setter method for _resolution property.
	 * @param value a String representing this item's resolution
	 */
	public void setMovieResolution(String value) { _resolution = value; }

	/**
	 * Frame rate of the movie.
	 */
	private String _frameRate = "";

	/**
	 * Getter method for _frameRate property.
	 * @return a String representing this item's frame rate
	 */
	public String getFrameRate() { return  _frameRate; }

	/**
	 * Setter method for _frameRate property.
	 * @param value a String representing this item's frame rate
	 */
	public void setFrameRate(String value) { _frameRate = value; }

	/**
	 * Movie runtime in minutes.
	 */
	private int _movieRuntime = 0;

	/**
	 * Getter method for _movieRuntime property.
	 * @return a value representing movie's runtime (in minutes)
	 */
	public int getMovieRuntime() { return _movieRuntime; }

	/**
	 * Setter method for _movieRuntime property.
	 * @param value a value representing movie's runtime (in minutes)
	 */
	public void setMovieRuntime(int value) { _movieRuntime = value; }

	/**
	 * Return the movie's plot / synopsis.
	 */
	private String _synopsis = "";              // LongDescription

	/**
	 * Getter method for _synopsis property.
	 * @return a String containing the plot of the movie
	 */
	public String getMovieSynopsis() { return _synopsis; }

	/**
	 * Setter method for _synopsis property.
	 * @param value a String containing the plot of the movie
	 */
	public void setMovieSynopsis(String value) { _synopsis = value; }

	/**
	 * Map of the Director(s) of the movie. The Map structure is
	 * <director_name, imdb_link>
	 */
	private Map<String,String> _directors;            // DirectorList --> DirectorName DirectorImdbCode DirectorImdbLink

	/**
	 * Getter method for _directors property.
	 * @return a Map object containing the list of directors
	 */
	public Map<String,String> getDirectors() {
		if (_directors == null) {
			_directors = new HashMap<>();
		}
		return _directors;
	}

	/**
	 * Add a director into the _directors Map.
	 * @param director name of the Director
	 * @param link link to IMDB page of the Director
	 */
	public void addDirector(String director, String link) {
		if (_directors == null) {
			_directors = new HashMap<>();
		}
		_directors.put(director, link);
	}

	/**
	 * Map of the Casts of the movie. The Map structure is
	 * <actor_name, <character_name, imdb_link>.
	 */
	private Map<String, Map<String,String>> _cast;           // CastList --> ActorName CharacterName ActorImdbCode ActorImdbLink

	/**
	 * Getter method for _cast property.
	 * @return a Map containing the casts data
	 */
	public Map<String,Map<String,String>> getCast() {
		if (_cast == null) {
			_cast = new HashMap<>();
		}
		return _cast;
	}

	/**
	 * Add a cast member into the _cast Map.
	 * @param actor actor's name
	 * @param character character's name
	 * @param link link to IMDB page of the actor
	 */
	public void addCast(String actor, String character, String link) {
		if (_cast == null) {
			_cast = new HashMap<>();
		}
		Map<String,String> c = new HashMap<>();
		c.put(character, link);
		_cast.put(actor, c);
	}

	/**
	 * URL to the screenshot image #1 on our cache server.
	 */
	private String _screenshot1 = "";

	/**
	 * Getter method for _screenshot1 property.
	 * @return the URL for screenshot#1
	 */
	public String getScreenshot1() { return _screenshot1; }

	/**
	 * Setter method for _screenshot1 property.
	 * @param value the URL for screenshot#1
	 */
	public void setScreenshot1(String value) { _screenshot1 = value; }

	/**
	 * URL to the screenshot image #2 on our cache server.
	 */
	private String _screenshot2 = "";

	/**
	 * Getter method for _screenshot2 property.
	 * @return the URL for screenshot#2
	 */
	public String getScreenshot2() { return _screenshot2; }

	/**
	 * Setter method for _screenshot2 property.
	 * @param value the URL for screenshot#2
	 */
	public void setScreenshot2(String value) { _screenshot2 = value; }

	/**
	 * URL to the screenshot image #3 on our cache server.
	 */
	private String _screenshot3 = "";

	/**
	 * Getter method for _screenshot3 property.
	 * @return the URL for screenshot#3
	 */
	public String getScreenshot3() { return _screenshot3; }

	/**
	 * Setter method for _screenshot3 property.
	 * @param value the URL for screenshot#3
	 */
	public void setScreenshot3(String value) { _screenshot3 = value; }
}
