/*
 * YIFYResponse.java
 *
 * Copyright (c) 2014 Ronald Kurniawan. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 */
package net.fluxo.blackpearl.dto;

import java.util.ArrayList;
import java.util.List;

/**
 * This is a Data Transfer Object representing a response from YIFY server.
 *
 * @author Ronald Kurniawan (viper)
 * @version 0.2.6, 1/04/14
 */
public class YIFYResponse {

	/**
	 * Total movie count that the server have.
	 */
	private int _movieCount = 0;

	public int getMovieCount() { return _movieCount; }
	public void setMovieCount(int value) {
		_movieCount = value;
	}

	/**
	 * Page number of the current response. This depends on the request sent that resulted
	 * with this response. The request should specify items per page.
	 */
	private int _page = 0;

	/**
	 * Getter method for _page property.
	 * @return page number of this response
	 */
	public int getPage() { return _page; }

	/**
	 * Setter method for _page property.
	 * @param value page number of this response
	 */
	public void setPage(int value) { _page = value; }

	/**
	 * List containing items returned in the response.
	 */
	private List<YIFYObject> _list;

	/**
	 * Return the list of items for this response.
	 * @return List object
	 */
	public List<YIFYObject> getMovies() {
		if (_list == null) {
			_list = new ArrayList<>();
		}
		return _list;
	}

	/**
	 * Set a list to be THE list of items for this response.
	 * @param list a List object containing all items
	 */
	public void setMovies(List<YIFYObject> list) {
		_list = list;
	}

	/**
	 * Add an item object to the list.
	 * @param obj item object to be added
	 */
	public void addMovieToList(YIFYObject obj) {
		if (_list == null) {
			_list = new ArrayList<>();
		}
		_list.add(obj);
	}
}
