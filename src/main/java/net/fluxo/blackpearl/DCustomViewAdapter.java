/*
 * DCustomViewAdapter.java
 *
 * Copyright (c) 2014 Ronald Kurniawan. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 */
package net.fluxo.blackpearl;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import net.fluxo.blackpearl.dto.DownloadProgress;

/**
 * BaseAdapter for Download Fragment.
 *
 * @author Ronald Kurniawan (viper)
 * @version 0.2.6, 8/05/14
 */
public class DCustomViewAdapter extends BaseAdapter {

	private Context _ctx = null;
	private Typeface _font = null;

	public DCustomViewAdapter(Context context, Typeface font) {
		_ctx = context;
		_font = font;
	}

	/**
	 * Returns the number of items actively downloading at the moment.
	 * @return number of items on download
	 */
	@Override
	public int getCount() {
		return Utils.getDownloadList().size();
	}

	/**
	 * Gets the object with index <i>i</i> from the download list.
	 * @param i index of object to be fetched
	 * @return the Object
	 */
	@Override
	public Object getItem(int i) {
		return null;
	}

	/**
	 * Returns item ID on the Object with index <i>i</i>.
	 * @param i index of object to be fetched
	 * @return item ID
	 */
	@Override
	public long getItemId(int i) {
		return 0;
	}

	/**
	 * Inflates the layout for this Fragment and sets the values inside the Fragment.
	 * @param i index of object to be fetched
	 * @param view a View object
	 * @param viewGroup a ViewGroup object
	 * @return the View object for the Layout of this Fragment
	 */
	@Override
	public View getView(int i, View view, ViewGroup viewGroup) {
		LayoutInflater inflater = (LayoutInflater)_ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		@SuppressLint({"ViewHolder", "InflateParams"}) View cellView = inflater.inflate(R.layout.progress_cell, null);
		if (cellView != null && Utils.getDownloadList().get(i) != null) {
			TextView tvTitle = (TextView)cellView.findViewById(R.id.tv_pg_title);
			if (_font != null) {
				tvTitle.setTypeface(_font);
			}
			DownloadProgress obj = Utils.getDownloadList().get(i);
			tvTitle.setText(obj.getTitle().equals("-") ? "Unknown Download" : obj.getTitle());
			TextView tvPercent = (TextView)cellView.findViewById(R.id.tv_pg_percent);
			if (_font != null) {
				tvPercent.setTypeface(_font);
			}
			tvPercent.setText(obj.getProgress() < 0 ? "N/A" : obj.getProgress() + "%");
			//notifyDataSetChanged();
		}
		return cellView;
	}
}
