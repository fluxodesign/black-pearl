/*
 * TPBCategoryAdapter.java
 *
 * Copyright (c) 2014 Ronald Kurniawan. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 */
package net.fluxo.blackpearl;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.*;

/**
 * BaseExpandableListAdapter for displaying TPB categories and its subcategories. This
 * class takes care the data when user checks or unchecks a checkbox.
 *
 * @author Ronald Kurniawan (viper)
 * @version 0.2.6, 21/04/14
 */
public class TPBCategoryAdapter extends BaseExpandableListAdapter {

	private Context _ctx;
	private List<String> _groups;
	private List<String> _selectedItems;

	public TPBCategoryAdapter(Context context) {
		_ctx = context;
		_groups = new ArrayList<>(Arrays.asList(_ctx.getResources().getStringArray(R.array.tpb_categories)));
		_selectedItems = new ArrayList<>();
	}

	/**
	 * Returns the List of selected subcategories.
	 * @return a List object
	 */
	public List<String> getSelectedSubcategories() {
		return _selectedItems;
	}

	/**
	 * Returns the number of groups from TPB.
	 * @return number of groups
	 */
	@Override
	public int getGroupCount() {
		if (_groups != null) {
			return  _groups.size();
		}
		return 0;
	}

	/**
	 * Returns the number of subcategories within a category.
	 * @param groupPosition index of category
	 * @return number of subcategories
	 */
	@Override
	public int getChildrenCount(int groupPosition) {
		String key = getGroup(groupPosition);
		return getSubcategories(key).size();
	}

	/**
	 * Returns a category name based on the position.
	 * @param groupPosition index of category
	 * @return name of category
	 */
	@Override
	public String getGroup(int groupPosition) {
		if (groupPosition < _groups.size()) {
			return _groups.get(groupPosition);
		}
		return null;
	}

	/**
	 * Returns the name of subcategory, based on category and subcategory positions.
	 * @param groupPosition index of category
	 * @param childPosition index of subcategory
	 * @return name of subcategory
	 */
	@Override
	public String getChild(int groupPosition, int childPosition) {
		String key = getGroup(groupPosition);
		List<String> subcats = getSubcategories(key);
		return subcats.get(childPosition);
	}

	/**
	 * Gets category ID based on group position.
	 * @param groupPosition index of category
	 * @return category ID
	 */
	@Override
	public long getGroupId(int groupPosition) {
		return groupPosition;
	}

	/**
	 * Gets subcategory ID based on category and subcategory positions.
	 * @param groupPosition index of category
	 * @param childPosition index of subcategory
	 * @return subcategory ID
	 */
	@Override
	public long getChildId(int groupPosition, int childPosition) {
		return childPosition;
	}

	@Override
	public boolean hasStableIds() {
		return false;
	}

	/**
	 * Inflates the layout for the TPB search category filter and populates the Dialog.
	 * @param groupPosition index of category
	 * @param isExpanded whether the category is expanded
	 * @param view a View object
	 * @param parent a ViewGroup object
	 * @return a View for the category
	 */
	@Override
	public View getGroupView(int groupPosition, boolean isExpanded, View view, ViewGroup parent) {
		LayoutInflater inflater = (LayoutInflater)_ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		@SuppressLint("InflateParams") View grView = inflater.inflate(R.layout.tpb_filter_group, null);
		if (_groups != null && grView != null) {
			TextView tvGroups = (TextView)grView.findViewById(R.id.tv_tpb_filter_group);
			tvGroups.setTypeface(Utils.getDefaultFont(_ctx));
			tvGroups.setText(getGroup(groupPosition));
			View groupIndicatorView = grView.findViewById(R.id.iv_tpb_filter_pointer);
			if (groupIndicatorView != null) {
				ImageView ivPointer = (ImageView)groupIndicatorView;
				if (getChildrenCount(groupPosition) > 0) {
					ivPointer.setVisibility(View.VISIBLE);
					ivPointer.setImageResource(isExpanded ? R.drawable.pointer_down : R.drawable.pointer_normal);
				} else {
					ivPointer.setVisibility(View.INVISIBLE);
				}
			}
		}
		return grView;
	}

	/**
	 * Inflates the layout for the TPB search subcategory filter and populates the layout.
	 * @param groupPosition index of category
	 * @param childPosition index of subcategory
	 * @param isLastChild whether the item is the last child
	 * @param view a View object
	 * @param parent a ViewGroup object
	 * @return a View for the subcategory
	 */
	@Override
	public View getChildView(final int groupPosition, final int childPosition, boolean isLastChild, View view, ViewGroup parent) {
		String subcat = getChild(groupPosition, childPosition);
		LayoutInflater inflater = (LayoutInflater)_ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		@SuppressLint("InflateParams") View cView = inflater.inflate(R.layout.tpb_filter_item, null);
		if (cView != null) {
			TextView tvTPBSubcat = (TextView)cView.findViewById(R.id.tv_tpb_filter_subcat);
			tvTPBSubcat.setTypeface(Utils.getDefaultFont(_ctx));
			tvTPBSubcat.setText(subcat);
			CheckBox cbTPBSubcat = (CheckBox)cView.findViewById(R.id.cb_tpb_filter_subcat);
			cbTPBSubcat.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View view) {
					boolean checked = ((CheckBox)view).isChecked();
					String subcatType = getSubcatType(groupPosition, childPosition);
					if (checked) {
						increase(subcatType);
					} else {
						decrease(subcatType);
					}
					if (_ctx != null && _ctx instanceof MainActivity) {
						CorsairFragment cFragment = ((MainActivity)_ctx).getCorsairFragment();
						if (cFragment != null) {
							cFragment.setSearchCategory(getProcessedSubcats());
						}
					}
				}
			});
			String subType = getType(groupPosition, childPosition);
			if (_selectedItems.contains(subType)) {
				cbTPBSubcat.setChecked(true);
			}
		}
		return cView;
	}

	@Override
	public boolean isChildSelectable(int groupPosition, int childPosition) {
		return true;
	}

	/**
	 * Return the subcategory name based on category and subcategory position.
	 *
	 * @param groupPosition index of category
	 * @param childPosition index of subcategory
	 * @return name of the subcategory
	 */
	private String getType(int groupPosition, int childPosition) {
		String group = getGroup(groupPosition);
		switch (group) {
			case "Audio": return _ctx.getResources().getStringArray(R.array.tpb_audio_type)[childPosition];
			case "Video": return _ctx.getResources().getStringArray(R.array.tpb_video_type)[childPosition];
			case "Applications": return _ctx.getResources().getStringArray(R.array.tpb_apps_type)[childPosition];
			case "Games": return _ctx.getResources().getStringArray(R.array.tpb_games_type)[childPosition];
			case "Porn": return _ctx.getResources().getStringArray(R.array.tpb_porn_type)[childPosition];
			case "Other": return _ctx.getResources().getStringArray(R.array.tpb_other_type)[childPosition];
		}
		return "";
	}

	/**
	 * Returns a List containing all the subcategory names contained in a category.
	 *
	 * @param category the name of category we'd like to get the subcategories of
	 * @return a List object
	 */
	private List<String> getSubcategories(String category) {
		List<String> list;
		switch (category) {
			case "Audio":
				list = new ArrayList<>(Arrays.asList(_ctx.getResources().getStringArray(R.array.tpb_audio)));
				break;
			case "Video":
				list = new ArrayList<>(Arrays.asList(_ctx.getResources().getStringArray(R.array.tpb_video)));
				break;
			case "Applications":
				list = new ArrayList<>(Arrays.asList(_ctx.getResources().getStringArray(R.array.tpb_apps)));
				break;
			case "Games":
				list = new ArrayList<>(Arrays.asList(_ctx.getResources().getStringArray(R.array.tpb_games)));
				break;
			case "Porn":
				list = new ArrayList<>(Arrays.asList(_ctx.getResources().getStringArray(R.array.tpb_porn)));
				break;
			case "Other":
				list = new ArrayList<>(Arrays.asList(_ctx.getResources().getStringArray(R.array.tpb_other)));
				break;
			default:
				list = new ArrayList<>();
				break;
		}
		return list;
	}

	/**
	 * Returns the subcategory code based on the category index and subcategory index.
	 *
	 * @param groupPosition index of category
	 * @param childPosition index of subcategory
	 * @return subcategory code
	 */
	private String getSubcatType(int groupPosition, int childPosition) {
		String key = getGroup(groupPosition);
		String value = "";
		List<String> list;
		switch (key) {
			case "Audio":
				list = new ArrayList<>(Arrays.asList(_ctx.getResources().getStringArray(R.array.tpb_audio_type)));
				value = list.get(childPosition);
				break;
			case "Video":
				list = new ArrayList<>(Arrays.asList(_ctx.getResources().getStringArray(R.array.tpb_video_type)));
				value = list.get(childPosition);
				break;
			case "Applications":
				list = new ArrayList<>(Arrays.asList(_ctx.getResources().getStringArray(R.array.tpb_apps_type)));
				value = list.get(childPosition);
				break;
			case "Games":
				list = new ArrayList<>(Arrays.asList(_ctx.getResources().getStringArray(R.array.tpb_games_type)));
				value = list.get(childPosition);
				break;
			case "Porn":
				list = new ArrayList<>(Arrays.asList(_ctx.getResources().getStringArray(R.array.tpb_porn_type)));
				value = list.get(childPosition);
				break;
			case "Other":
				list = new ArrayList<>(Arrays.asList(_ctx.getResources().getStringArray(R.array.tpb_other_type)));
				value = list.get(childPosition);
				break;
		}
		return value;
	}

	/**
	 * Removes the value from the selected items.
	 *
	 * @param value value to be removed from the list
	 */
	private void decrease(String value) {
		if (_selectedItems != null && _selectedItems.size() > 0 && _selectedItems.contains(value)) {
			_selectedItems.remove(value);
		}
	}

	/**
	 * Adds the value into the list of selected items.
	 *
	 * @param value value to be added to the list
	 */
	private void increase(String value) {
		if (_selectedItems != null && !_selectedItems.contains(value)) {
			_selectedItems.add(value);
		}
	}

	/**
	 * Gets all the subcategories.
	 * @return a String containing all user-selected categories, shortened if applicable
	 * @see compareFullToPartialList()
	 */
	private String getProcessedSubcats() {
		StringBuilder sb = new StringBuilder();
		// Audio...
		List<String> list = new ArrayList<>(Arrays.asList(_ctx.getResources().getStringArray(R.array.tpb_audio_type)));
		List<String> result = compareFullToPartialList(list, "100");
		for (String s : result) {
			sb.append(s).append(",");
		}
		// Video...
		list = new ArrayList<>(Arrays.asList(_ctx.getResources().getStringArray(R.array.tpb_video_type)));
		result = compareFullToPartialList(list, "200");
		for (String s: result) {
			sb.append(s).append(",");
		}
		// Applications...
		list = new ArrayList<>(Arrays.asList(_ctx.getResources().getStringArray(R.array.tpb_apps_type)));
		result = compareFullToPartialList(list, "300");
		for (String s : result) {
			sb.append(s).append(",");
		}
		// Games...
		list = new ArrayList<>(Arrays.asList(_ctx.getResources().getStringArray(R.array.tpb_games_type)));
		result = compareFullToPartialList(list, "400");
		for (String s : result) {
			sb.append(s).append(",");
		}
		// Porn...
		list = new ArrayList<>(Arrays.asList(_ctx.getResources().getStringArray(R.array.tpb_porn_type)));
		result = compareFullToPartialList(list, "500");
		for (String s : result) {
			sb.append(s).append(",");
		}
		// Other...
		list = new ArrayList<>(Arrays.asList(_ctx.getResources().getStringArray(R.array.tpb_other_type)));
		result = compareFullToPartialList(list, "600");
		for (String s : result) {
			sb.append(s).append(",");
		}

		return sb.toString().substring(0, sb.length()-1);
	}

	/**
	 * This method replaces the top category codes with all subcategory codes underneath it. For example if 100 is found,
	 * we replace it with 101, 102, 103, 104 and 199.
	 *
	 * @param raw the final selected subcategories separated by commas
	 */
	public void deprocessSubcats(String raw) {
		_selectedItems.clear();
		StringTokenizer tokenizer = new StringTokenizer(raw, ",");
		while (tokenizer.hasMoreElements()) {
			String s = tokenizer.nextToken();
			switch (s) {
				case "100":
					Collections.addAll(_selectedItems, _ctx.getResources().getStringArray(R.array.tpb_audio_type));
					break;
				case "200":
					Collections.addAll(_selectedItems, _ctx.getResources().getStringArray(R.array.tpb_video_type));
					break;
				case "300":
					Collections.addAll(_selectedItems, _ctx.getResources().getStringArray(R.array.tpb_apps_type));
					break;
				case "400":
					Collections.addAll(_selectedItems, _ctx.getResources().getStringArray(R.array.tpb_games_type));
					break;
				case "500":
					Collections.addAll(_selectedItems, _ctx.getResources().getStringArray(R.array.tpb_porn_type));
					break;
				case "600":
					Collections.addAll(_selectedItems, _ctx.getResources().getStringArray(R.array.tpb_other_type));
					break;
				default:
					_selectedItems.add(s);
					break;
			}
		}
	}

	/**
	 * This method compares the selected items with the full list of subcategories. If every subcategories inside a
	 * category is selected, we should just include the category number. For example, if subcats 101, 102, 103, 104
	 * and 199 were selected, we replaces them with 100.
	 *
	 * @param fullList the full list to be compared
	 * @param allCatsType the all-encompassing category number that should replace all other subcategories
	 * @return new List object
	 */
	private List<String> compareFullToPartialList(List<String> fullList, String allCatsType) {
		List<String> tempList = new ArrayList<>();
		boolean isAllIncluded = true;
		for (String s : fullList) {
			if (!_selectedItems.contains(s)) {
				isAllIncluded = false;
			} else {
				tempList.add(s);
			}
		}
		List<String> finalList = new ArrayList<>();
		if (isAllIncluded) {
			finalList.add(allCatsType);
		} else {
			finalList = tempList;
		}
		return finalList;
	}
}
