/*
 * Utils.java
 *
 * Copyright (c) 2014 Ronald Kurniawan. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 */
package net.fluxo.blackpearl;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Vibrator;
import android.preference.PreferenceManager;
import android.util.Log;
import net.fluxo.blackpearl.dto.*;
import org.apache.commons.codec.binary.Hex;
import org.apache.commons.codec.digest.DigestUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Utility class that houses all the methods that can be called from anywhere in the App.
 *
 * @author Ronald Kurniawan (viper)
 * @version 0.2.6, 29/03/2014
 */
public class Utils {

	private static Typeface _t = null;

	/**
	 * A List that holds the downloads that are running at the moment.
	 */
	private static List<DownloadProgress> _downloads = new ArrayList<>();

	/**
	 * Checks whether an active internet connection is present.
	 * @param c Context object
	 * @return true if internet connection is available; false otherwise
	 */
	public static boolean isConnectedToInternet(Context c) {
		ConnectivityManager cm = (ConnectivityManager)c.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
		return (activeNetwork != null && activeNetwork.isConnectedOrConnecting());
	}

	/**
	 * Checks whether a Wifi connection is available.
	 * @param c Context object
	 * @return true if wifi connection is available; false otherwise
	 */
	public static boolean isWifiConnected(Context c) {
		ConnectivityManager cm = (ConnectivityManager)c.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
		return (activeNetwork != null && activeNetwork.getType() == ConnectivityManager.TYPE_WIFI);
	}

	/**
	 * Vibrates the device for 100ms.
	 * @param c Context object
	 */
	public static void vibrate(Context c) {
		Vibrator v = (Vibrator)c.getSystemService(Context.VIBRATOR_SERVICE);
		v.vibrate(100);
	}

	/**
	 * Getter method for _downloads property.
	 * @return List object containing all the running downloads at the moment
	 */
	public static List<DownloadProgress> getDownloadList() {
		return _downloads;
	}

	/**
	 * Formats the returned download progress data and update the Download Fragment.
	 * @param mainActivity MainActivity object
	 * @param raw raw String data from server
	 */
	public static void processAndUpdateDownloadProgressData(MainActivity mainActivity, String raw) {
		List<DownloadProgress> list = new ArrayList<>();
		try {
			JSONObject json = new JSONObject(raw);
			JSONArray items = json.getJSONArray("Progress");
			for (int i = 0; i < items.length(); i++) {
				JSONObject item = items.getJSONObject(i);
				Iterator itKeys = item.keys();
				while (itKeys.hasNext()) {
					String key = String.valueOf(itKeys.next());
					if (item.getInt(key) >= -1) {
						DownloadProgress dp = new DownloadProgress(key, item.getInt(key));
						list.add(dp);
					}
				}
			}
		} catch (Exception e) {
			Log.e("FREEBOOTER", "Error processing download progress data");
			Log.e("FREEBOOTER", e.getMessage());
		}
		_downloads = list;
		// if the download list is not empty, set another progress update alarm...
		if (_downloads.size() > 0) {
			mainActivity.setProgressAlarm();
			mainActivity.startDownloadProgressUpdaterThread();
		} else {
			mainActivity.cancelProgressAlarm();
		}
	}

	/**
	 * Returns the default Typeface used in this App.
	 * @param c Context object
	 * @return the default Typeface
	 */
	public static Typeface getDefaultFont(Context c) {
		if (_t == null) {
			try {
				_t = Typefaces.get(c, "ubuntu");
			} catch (Exception e) {
				Log.e("FREEBOOTER", "Error getting font asset!");
				Log.e("FREEBOOTER", e.getMessage());
				_t = null;
			}
		}
		return _t;
	}

	/**
	 * Generate and save a unique Chat ID in the SharedPreferences (if none are found).
	 * @param ctx Context object
	 */
	public static void generateChatID(Context ctx) {
		SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(ctx);
		if (pref.getString("freebooter_pref_chat_genID", "").length() == 0) {
			// Generate an ID...
			String raw = pref.getString("freebooter_pref_chat_username", "username") + "/" +
				pref.getString("freebooter_pref_network", Long.toString(Calendar.getInstance().getTimeInMillis()));
			String genID = new String(Hex.encodeHex(DigestUtils.md5(raw)));
			pref.edit().putString("freebooter_pref_chat_genID", genID).commit();
		}
	}

	/**
	 * Returns the name of a TPB category based on its code.
	 * @param cat category code
	 * @return TPB category name
	 */
	public static String getTPBCategory(int cat) {
		String response = "Unknown";
		switch (cat) {
			case 0: response = "All"; break;
			case 100: response = "Audio"; break;
			case 101: response = "Music"; break;
			case 102: response = "Audio Book"; break;
			case 103: response = "Sound Clip"; break;
			case 104: response = "FLAC"; break;
			case 200: response = "Video"; break;
			case 201: response = "Movie"; break;
			case 202: response = "DVDR"; break;
			case 203: response = "Music Video"; break;
			case 204: response = "Movie Clip"; break;
			case 205: response = "TV Show"; break;
			case 206: response = "Handheld"; break;
			case 207: response = "HD Movie"; break;
			case 208: response = "HD TV Show"; break;
			case 209: response = "3D"; break;
			case 300: response = "Application"; break;
			case 301: response = "Windows"; break;
			case 302: response = "Mac"; break;
			case 303: response = "UNIX"; break;
			case 304: response = "Handheld"; break;
			case 305: response = "iOS"; break;
			case 306: response = "Android"; break;
			case 400: response = "Game"; break;
			case 401: response = "PC Game"; break;
			case 402: response = "Mac Game"; break;
			case 403: response = "PSx Game"; break;
			case 404: response = "XBox 360"; break;
			case 405: response = "Wii Game"; break;
			case 406: response = "Handheld Game"; break;
			case 407: response = "iOS Game"; break;
			case 408: response = "Android Game"; break;
			case 500: response = "Porn"; break;
			case 501: response = "X Movie"; break;
			case 502: response = "X DVDR"; break;
			case 503: response = "X Picture"; break;
			case 504: response = "X Game"; break;
			case 505: response = "X HD Movie"; break;
			case 506: response = "X Movie Clip"; break;
			case 601: response = "e-Book"; break;
			case 602: response = "Comic"; break;
			case 603: response = "Picture"; break;
			case 604: response = "Cover"; break;
			case 605: response = "Physible"; break;
			case 199:
			case 299:
			case 399:
			case 499:
			case 599:
			case 699: response = "Other"; break;
		}
		return response;
	}

	/**
	 * Formats a raw JSON response containing YIFY list, and returns a YIFYResponse object.
	 * @param raw raw JSON object
	 * @return a YIFYResponse object
	 */
	public static YIFYResponse processJSON(String raw) {
		YIFYResponse response = new YIFYResponse();
		try {
			JSONObject json = new JSONObject(raw);
			response.setMovieCount(Integer.parseInt(json.getString("MovieCount")));
			JSONArray movies = json.getJSONArray("MovieList");
			for (int i = 0; i < movies.length(); i++) {
				YIFYObject yo = new YIFYObject();
				JSONObject movie = movies.getJSONObject(i);
				yo.setMovieID(movie.getString("MovieID"));
				switch (movie.getString("State")) {
					case "Disabled": yo.setMovieState(YIFYObject.MovieState.DISABLED); break;
					case "Removed Due to DMCA": yo.setMovieState(YIFYObject.MovieState.REMOVED_DMCA); break;
					case "OK":
					default: yo.setMovieState(YIFYObject.MovieState.OK); break;
				}
				yo.setMovieURL(movie.getString("MovieUrl"));
				yo.setMovieTitle(movie.getString("MovieTitleClean"));
				yo.setMovieYear(movie.getString("MovieYear"));
				yo.setDateUploaded(movie.getString("DateUploaded"));
				yo.setDateUploadedMillis(movie.getLong("DateUploadedEpoch"));
				yo.setMovieQuality(movie.getString("Quality"));
				yo.setCoverURL(movie.getString("CoverImage"));
				yo.setIMDBCode(movie.getString("ImdbCode"));
				yo.setIMDBURL(movie.getString("ImdbLink"));
				yo.setMovieSize(movie.getString("Size"));
				yo.setMovieSizeInBytes(movie.getLong("SizeByte"));
				yo.setMovieRating(movie.getString("MovieRating"));
				yo.setMovieGenre(movie.getString("Genre"));
				yo.setMovieUploader(movie.getString("Uploader"));
				yo.setDownloadedTimes(movie.getInt("Downloaded"));
				yo.setTorrentSeeds(movie.getInt("TorrentSeeds"));
				yo.setTorrentPeers(movie.getInt("TorrentPeers"));
				yo.setTorrentURL(movie.getString("TorrentUrl"));
				yo.setTorrentHash(movie.getString("TorrentHash"));
				yo.setTorrentMagnetURL(movie.getString("TorrentMagnetUrl"));
				response.addMovieToList(yo);
			}
		} catch (JSONException jse) {
			Log.e("FREEBOOTER", "Error processing YIFY's JSON response!");
			Log.e("FREEBOOTER", jse.getMessage());
		}
		return response;
	}

	/**
	 * Formats a raw JSON response containing YIFY item details, and returns a YIFYObjectDetails object.
	 * @param raw raw JSON object
	 * @return a YIFYObjectDetails object
	 */
	public static YIFYObjectDetails processDetailsJSON(String raw) {
		YIFYObjectDetails obj = new YIFYObjectDetails();
		try {
			JSONObject json = new JSONObject(raw);
			obj.setMovieID(json.getString("MovieID"));
			obj.setMovieURL(json.getString("MovieUrl"));
			obj.setMovieTitle(json.getString("MovieTitleClean"));
			obj.setMovieYear(json.getString("MovieYear"));
			obj.setDateUploaded(json.getString("DateUploaded"));
			obj.setDateUploadedMillis(json.getLong("DateUploadedEpoch"));
			obj.setMovieQuality(json.getString("Quality"));
			obj.setFrameRate(json.getString("FrameRate"));
			obj.setMovieResolution(json.getString("Resolution"));
			obj.setMovieRuntime(Integer.parseInt(json.getString("MovieRuntime")));
			obj.setMovieSynopsis(json.getString("LongDescription"));
			obj.setCoverURL(json.getString("MediumCover"));
			obj.setIMDBCode(json.getString("ImdbCode"));
			obj.setIMDBURL(json.getString("ImdbLink"));
			obj.setMovieSize(json.getString("Size"));
			obj.setMovieSizeInBytes(json.getLong("SizeByte"));
			obj.setMovieRating(json.getString("MovieRating"));
			String genre = json.getString("Genre1");
			if (json.getString("Genre2") != null && json.getString("Genre2").length() > 0 && !json.getString("Genre2").equals("null")) {
				genre += " | " + json.getString("Genre2");
			}
			obj.setMovieGenre(genre);
			obj.setMovieUploader(json.getString("Uploader"));
			obj.setDownloadedTimes(json.getInt("Downloaded"));
			obj.setTorrentSeeds(json.getInt("TorrentSeeds"));
			obj.setTorrentPeers(json.getInt("TorrentPeers"));
			obj.setTorrentURL(json.getString("TorrentUrl"));
			obj.setTorrentHash(json.getString("TorrentHash"));
			obj.setTorrentMagnetURL(json.getString("TorrentMagnetUrl"));
			obj.setScreenshot1(json.getString("MediumScreenshot1"));
			obj.setScreenshot2(json.getString("MediumScreenshot2"));
			obj.setScreenshot3(json.getString("MediumScreenshot3"));
			JSONArray directorsArray = json.getJSONArray("DirectorList");
			for (int i = 0; i < directorsArray.length(); i++) {
				JSONObject dirObj = directorsArray.getJSONObject(i);
				obj.addDirector(dirObj.getString("DirectorName"), dirObj.getString("DirectorImdbLink"));
			}
			JSONArray castArray = json.getJSONArray("CastList");
			for (int i = 0; i< castArray.length(); i++) {
				JSONObject castObj = castArray.getJSONObject(i);
				obj.addCast(castObj.getString("ActorName"), castObj.getString("CharacterName"),
					castObj.getString("ActorImdbLink"));
			}
		} catch(JSONException jse) {
			Log.e("FREEBOOTER", "Error processing YIFY's Detail JSON response!");
			Log.e("FREEBOOTER", jse.getMessage());
		}
		return obj;
	}

	/**
	 * Formats a raw JSON response from TPB search request, and returns a TPBResponse object.
	 * @param raw raw JSON object
	 * @return a TPBResponse object
	 */
	public static TPBResponse processTPBDetailsJSON(String raw) {
		TPBResponse tpbResponse = new TPBResponse();
		try {
			JSONObject json = new JSONObject(raw);
			tpbResponse.setItemCount(Integer.parseInt(json.getString("_totalItems")));
			JSONArray items = json.getJSONArray("_tpbItems");
			for (int i = 0; i < items.length(); i++) {
				TPBObjectDetails tpbo = new TPBObjectDetails();
				JSONObject item = items.getJSONObject(i);
				tpbo.setSeeders(item.getInt("_seeders"));
				tpbo.setLeechers(item.getInt("_leechers"));
				tpbo.setType(item.getInt("_type"));
				tpbo.setTitle(item.getString("_title"));
				tpbo.setUploaded(item.getString("_uploaded"));
				tpbo.setSize(item.getString("_size"));
				tpbo.setUploader(item.getString("_uploadedBy"));
				tpbo.setDetailsURL(item.getString("_detailsURL"));
				tpbo.setMagnetURL(item.getString("_magnetURL"));
				tpbo.setTorrentURL(item.getString("_torrentURL"));
				tpbResponse.addItemsToList(tpbo);
			}
		} catch (JSONException jse) {
			Log.e("FREEBOOTER", "Error processing TPB Object JSON response!");
			Log.e("FREEBOOTER", jse.getMessage());
		}
		return tpbResponse;
	}

	/**
	 * Formats a raw JSON object containing TPB item details, and returns a TPBObjectDetails object.
	 * @param raw raw JSON object
	 * @param context Context object
	 * @return a TPBObjectDetails object
	 */
	public static TPBObjectDetails processTPBInfoJSON(String raw, Context context) {
		TPBObjectDetails t = null;
		try {
			JSONObject json = new JSONObject(raw);
			String url = json.getString("_request");
			String info = json.getString("_info");
			CorsairFragment cFragment = ((MainActivity)context).getCorsairFragment();
			if (cFragment != null) {
				t = cFragment.findObjectByDetailsURL(url);
				if (t != null) {
					t.setInfo(info);
				}
			}
		} catch (JSONException jse) {
			Log.e("FREEBOOTER", "Error processing TPB Info JSON response!");
			Log.e("FREEBOOTER", jse.getMessage());
		}
		return t;
	}

	private static String[] _arrTPBResults = null;
	private static int _tpbrEnd = -1;

	/**
	 * This method processes a series of TPB request chat messages from the daemon. A standard raw JSON response containing
	 * TPB queries can be very long and will not fit in a single XMPP message, so the daemon divides the message into
	 * 4000-char long messages, tag the messages with serial number and send them. This method captures all the messages and
	 * rearrange them properly, process and return them as a TPBResponse object.
	 *
	 * @param message message to be processed
	 * @return a TPBResponse object
	 */
	public static TPBResponse processTPBXMPPResult(String message) {
		Pattern pattern = Pattern.compile("^[\\d]+\\/[\\d]+[\\:]{3}");
		Matcher m = pattern.matcher(message);
		if (m.find()) {
			String tag = m.group();
			tag = tag.replace(":::", "");
			StringTokenizer tokenizer = new StringTokenizer(tag, "/");
			int start = Integer.parseInt(tokenizer.nextToken());
			int end = Integer.parseInt(tokenizer.nextToken());
			if (_tpbrEnd < 0) {
				_tpbrEnd = end;
			}
			if (_arrTPBResults == null) {
				_arrTPBResults = new String[_tpbrEnd];
			}
			String content = m.replaceFirst("");
			// "Slot" the message
			if (_tpbrEnd == end) {
				_arrTPBResults[start-1] = content;
			}
			// Now check if all slots have been filled
			int count = 0;
			for (String arrTPBResult : _arrTPBResults) {
				if (arrTPBResult != null) {
					count++;
				}
			}
			if (count == _arrTPBResults.length) {
				StringBuilder sb = new StringBuilder();
				for (String s : _arrTPBResults) {
					sb.append(s);
				}
				TPBResponse tpbResponse = Utils.processTPBDetailsJSON(sb.toString());
				_tpbrEnd = -1;
				_arrTPBResults = null;
				return tpbResponse;
			}
		}
		return null;
	}

	/**
	 * Returns the generated Chat ID saved in the SharedPreferences.
	 * @param context Context object
	 * @return the generated Chat ID
	 */
	public static String getGeneratedID(MainActivity context) {
		SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(context);
		return pref.getString("freebooter_pref_chat_genID", "NO-ONWER-ID");
	}

	/**
	 * Returns a SHA-256 Hash value from a string (in hex values).
	 * @param original the String to derive the hash from
	 * @return a hash value
	 */
	public static String getHashValue(String original) {
		try {
			MessageDigest md = MessageDigest.getInstance("SHA-256");
			md.update(original.getBytes());
			byte[] digested = md.digest();
			StringBuilder sb = new StringBuilder();
			for (byte aDigested : digested) {
				String hex = Integer.toHexString(0xff & aDigested);
				if ((hex.length()) == 1) {
					sb.append('0');
				}
				sb.append(hex);
			}
			return sb.toString();
		} catch (NoSuchAlgorithmException nsae) {
			Log.e("FREEBOOTER", "Failed to hash a string: " + nsae.getMessage());
		}
		return original;
	}
}
